<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancellation extends Model
{
	/**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
	public $timestamps = true;


	protected $fillable = [
	'final_cancel_day', 'reason', 'event_id'
	];


	public function event() {
		return $this->belongsTo('App\Event');
	}
	
}
