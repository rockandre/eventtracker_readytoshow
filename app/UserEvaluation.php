<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEvaluation extends Model
{
    public $timestamps = true;

    protected $table = 'user_evaluations';

    protected $fillable = [
    	'event_id', 'user_id'
    ];

    public function event()
 	{
 		$this->belongsTo('App\Event');
 	}

 	public function user()
 	{
 		$this->belongsTo('App\User');
 	}
}
