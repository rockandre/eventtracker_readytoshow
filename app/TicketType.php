<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
	public $timestamps = true;

	protected $table = 'ticket_types';
	
	protected $fillable = [
		'event_id', 'name', 'quantity', 'price', 'ageRestrictionMin', 'ageRestrictionMax', 'is_standard',
	];

	public function event()
	{
		return $this->belongsTo('App\Event');
	}

	public function tickets()
	{
		return $this->hasMany('App\Ticket');
	}
}
