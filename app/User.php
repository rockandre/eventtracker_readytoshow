<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;



    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'email', 'password', 'address', 'NIF', 'gender', 'district_id', 'birthdate', 'social_account', 'is_admin', 'email_token', 'confirmed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password'
    ];

    public function events() {
        return $this->hasMany('App\Event');
    }

    public function district() {
        return $this->belongsTo('App\District');
    }

    public function followingEvents()
    {
        return $this->belongsToMany('App\Event', 'event_followers');
    }

    public function promotorEvals()
    {
        return $this->hasMany('App\Evaluation', 'promotor_id');
    }

    public function evaluations()
    {
        return $this->hasMany('App\UserEvaluation');
    }

    public function isAdmin() 
    {
        if($this->is_admin == 1) {
            return true;
        }        

        return false;
    }

    public function haveEvents()
    {
        if($this->events()->where('status', 1)->count() > 0 ) {
            return true;
        }

        return false;
    }

    public function genderToStr()
    {
        $genderStr="";

        switch ($this->gender) {
            case 'M':
            $genderStr="Masculino";
            break;
            case 'F':
            $genderStr="Feminino";
            break;
            case 'O':
            $genderStr="Outro";
            break;
        }

        return $genderStr;
    }

    public function dateToString()
    {
        $date = Carbon::parse($this->birthdate);

        return $date->format('d/m/Y');
    }


    public function orders(){
        return $this->hasMany('App\Order');
    }
}
