<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $timestamps = true;

	protected $table = 'reports';

	protected $fillable = [
		'event_id', 'reason', 'reviewed',
	];

	public function event()
	{
		return $this->belongsTo('App\Event');
	}
}
