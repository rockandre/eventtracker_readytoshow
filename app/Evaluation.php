<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    public $timestamps = true;

    protected $table = 'evaluations';

    protected $fillable = [
    'event_id', 'user_id', 'promotor_id', 'eventEval', 'eventOpinion', 'promotorEval', 'promotorOpinion',
    ];

    public function event()
 	{
 		return $this->belongsTo('App\Event');
 	}

 	public function promotor()
 	{
 		return $this->belongsTo('App\User', 'promotor_id');
 	}
}