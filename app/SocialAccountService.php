<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::whereProvider($provider)
        ->whereProviderUserId($providerUser->getId())
        ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
                ]);
            
            $providerId = $provider=='facebook' ? 1 : 2;

            $user = User::where('email', $providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'social_account' => $provider == 'facebook' ? 1 : 2,
                    'email_token' => 'ACTIVATED',
                    'confirmed' => 1,
                    ]);

                $user->image_id = $this->getSocialAccountImage($providerUser, $provider, $user->id);
                $user->save();
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }

    public function createOrUpdateTempUser(ProviderUser $providerUser)
    {
        $account = FacebookTempAccount::where('provider_user_id', $providerUser->getId())->first();

        if($account) {
            $user = $account;
        }
        else {
            $user = FacebookTempAccount::create([
                'provider_user_id' => $providerUser->getId(),
                'name' => $providerUser->getName()]);
        }

        return $user;
    }

    public function checkAccountAlreadyExists(ProviderUser $providerUser) 
    {
        $account = SocialAccount::where('provider_user_id', $providerUser->getId())->first();

        if(!is_null($account)) {
            return false;
        } else {
            return true;
        }
    }

    private function getSocialAccountImage($providerUser, $provider, $userId){

        if($provider == 'facebook')
        {
            $url = "https://graph.facebook.com/".$providerUser->getId()."/picture?width=400";
        } 
        else{

            $url = substr($providerUser->getAvatar(), 0, -2) . '400';
        }

        $fileName = $userId . '_' . time() . '.png';

        Image::make($url)->heighten(400)->save(storage_path('/app/public/profile/' . $fileName), 100);

        return $fileName;
    }

    public function getFacebookAccountImage($providerId, $userId)
    {
        $url = "https://graph.facebook.com/".$providerId."/picture?width=400";

        $fileName = $userId . '_' . time() . '.png';

        Image::make($url)->heighten(400)->save(storage_path('/app/public/profile/' . $fileName), 100);

        return $fileName;
    }
}