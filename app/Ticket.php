<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	public $timestamps = true;

	protected $table = 'tickets';
	
	protected $fillable = [
		'ticket_type_id', 'order_id', 'email', 'owner_name', 'identification_number', 'transmissable', 'private_code'
	];

	public function ticketType()
 	{
 		return $this->belongsTo('App\TicketType');
 	}

 	public function order()
 	{
 		return $this->belongsTo('App\Order');
 	}
}
