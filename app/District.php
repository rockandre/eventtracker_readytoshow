<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function events() {
    	return $this->hasMany('App\Event');
    }

    public function users() {
    	return $this->hasMany('App\User');
    }
}
