<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use App\Event;

class OwnerUserOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = Event::findOrFail($request->route()->id);

        if($request->user()->id == $event->user_id) {
            return $next($request);
        }
        
        return redirect('event/'.$request->route()->id); 
    }
}
