<?php

namespace App\Http\Middleware;

use Closure;

use App\Event;

use Carbon\Carbon;


class CancelEvent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = Event::findOrFail($request->route()->id);

        if(($request->user()->id != $event->user_id) || $event->status >= 2 )
        {
            if(!$request->ajax())
            {
                return redirect('home');
            }
            else
            {
                abort(403, 'Unauthorized action.');
            }
            
        }
        else{
            $date = Carbon::parse($event->initDate);

            if(Carbon::now() > $date) {

                return redirect('home');
            }
        }



        return $next($request);
    }
}
