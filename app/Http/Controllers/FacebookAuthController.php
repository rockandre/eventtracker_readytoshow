<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SocialAccountService;

use Socialite;

class FacebookAuthController extends SocialAuthController
{
    const PROVIDER = 'facebook';

    public function callback(Request $request, SocialAccountService $service)
    {   
        if(!$request->has('code'))
        {
            return redirect()->to('/login');
        }

        $providerUser = Socialite::driver(self::PROVIDER)->user();

        if(is_null($providerUser->getEmail()) && $service->checkAccountAlreadyExists($providerUser))
        {
            $user = $service->createOrUpdateTempUser($providerUser);
            return redirect()->to('/register/setFacebookEmail/'.$user->id);
        }

        $user = $service->createOrGetUser($providerUser, self::PROVIDER);

        auth()->login($user);

        return redirect()->to('/');
    }
}
