<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\User;
use App\Order;
use App\Event;
use App\District;	
use Carbon\Carbon;

class UserController extends Controller
{
	public function show ($id) 
	{
		$user = User::findOrFail($id);

		return view('users.show', compact('user'));
	}

	public function profile ()
	{
		$currentView = 'profile';

		$user = Auth::user();

		$districts = District::all();

		return view('users.profile', compact('currentView', 'user', 'districts'));
	}

	public function orders (){

		$currentView = 'orders';

		return view('users.orders', compact('currentView'));
	}

	public function following (){

		$currentView = 'following';

		return view('users.following', compact('currentView'));
	}
	public function showChangePassword (){

		$currentView = 'changePassword';

		return view('users.changePassword', compact('currentView'));
	}

	public function update(Request $request) {

		$this->validator($request->all())->validate();

		$this->updateUser(Auth::user(), $request->all());

		return ['redirect' => url('/user/personal/profile')];

	}

	public function storeNewPassword(Request $request){

		$user = Auth::user();
		$password = $user->getAuthPassword();

		$errors = "";

		foreach($request->all() as $value)
		{
			if(strlen($value) == 0)
			{
				return response()->json(['errors' => 'ola'], 422);
			}
		}

		$this->changePasswordValidator($request->all())->validate();
		
		if(Hash::check($request['oldPassword'], $password)) {
			if(!Hash::check($request['password'], $password)) {
				if($request['password'] == $request['confirm']) {
					$user->password = bcrypt($request['password']);
					$user->save();
				} 
				else {
					return response()->json(['errors' => 'Os campos Palavra Passe e Confirmação de Palavra Passe não coincidem'], 422);
				}
			}
			else {
				return response()->json(['errors' => 'A nova Palavra passe terá de ser diferente da antiga.'], 422);
			}
		}
		else {
			return response()->json(['errors' => 'A Palavra Passe Antiga não coincide com os nossos registos.'], 422);
		}

		return response()->json('Success', 200);

	}

	public function updateUser($user, $data)
	{
		$user->nif = isset($data['nif']) ? $data['nif'] : null;
		$user->gender = isset($data['gender']) ? $data['gender'] : null;
		$user->birthdate = isset($data['birthdate']) && $data['birthdate']!=false ? Carbon::parse($data['birthdate']) : null;
		$user->description = isset($data['description']) ? $data['description'] : null;
		$user->district_id = $data['district'];
		$user->address = isset($data['address']) ? $data['address'] : null;

		if(!is_null($data['profileImage']) && $data['profileImage'] != 'DEFAULT')
		{
			$user->image_id = $this->generateProfileImage($user->id, $data['profileImage']);

		} else {
			if($data['profileImage'] == 'DEFAULT')
			{
				$user->image_id = 'default.jpg';
			}
		}

		$user->save();
	}

	private function generateProfileImage($userId, $image)
	{

		$fileName = $userId . '_' . time() . '.png';

		$image = Image::make($image);
		$image->heighten(400);
		$image->save(storage_path('/app/public/profile/' . $fileName), 100);

		return $fileName;
	}


	protected function validator(array $data)
	{   

		$rules = array (
			'address' => 'nullable|max:255',
			'nif' => 'nullable|digits:9|unique:users,email,'.Auth::user()->id,
			'description' => 'nullable|max:1000',
			'gender' => 'nullable|in:M,F,O',);

		$messages = array (
			'digits' => 'O :attribute tem de ter exatamente :digits carateres',
			'nif.unique' => 'O NIF fornecido já se encontra em utilização',
			'max' => 'O campo :attribute poderá ter no maximo :max carateres',
			'min' => 'O campo :attribute tem de ter pelo menos :min carateres',
			'gender.in' => 'O campo género definido não corresponde aos existentes',
			);	

		return Validator::make($data, $rules, $messages);
	}


	protected function changePasswordValidator (array $data)
	{

		$rules = array (
			'password' => 'min:8',
			'oldPassword' => 'min:8',
			);


		$messages = array ( 'min' => 'Uma password deverá ter no minimo 8 carateres');

		return Validator::make($data, $rules, $messages);
	}

	public function toggleFollow($event_id)
	{
		$user = Auth::user();

		if($user->followingEvents()->where('event_id', $event_id)->count())
		{
			$user->followingEvents()->detach($event_id);

			$user->save();
		}
		else {
			$user->followingEvents()->attach($event_id);

			$user->save();
		}
	}

	public function getFollowing(Request $request)
	{
		$user = $request->user();

		$queryResults = $user->followingEvents()
		->where('events.hidden', '=', 0)
		->leftJoin('categories', 'events.category_id', '=', 'categories.id')
		->orderBy('events.'.$request['orderByParam'], $request['orderByType'])
		->select('events.*', 'categories.categoryName')
		->paginate(10);


		for($i = 0; $i < sizeof($queryResults); $i ++)
		{
			$queryResults[$i]['tags'] = $queryResults[$i]->tags()->pluck('tag');
		}
		
		return $queryResults;
	}


	public function getOrders(Request $request)
	{
		$user = $request->user();


		$table = ($request['orderByParam'] == 'initDate' || $request['orderByParam'] == 'name') ? 'events' : 'orders';

		$queryResults = $user->orders()
		->leftJoin('events', 'orders.event_id', '=', 'events.id')
		->leftJoin('districts', 'events.district_id', '=', 'districts.id')
		->leftJoin('categories', 'events.category_id', '=', 'categories.id')
		->orderBy($table.'.'.$request['orderByParam'], $request['orderByType'])
		->select('categories.categoryName as category','orders.created_at', 'events.name', 'districts.name as district','events.image_id','events.initDate', 'orders.ammount', 'orders.event_id', 'orders.tickets', 'orders.created_at')
		->paginate(10);

		for($i = 0; $i < sizeof($queryResults); $i ++)
		{
			$queryResults[$i]['tags'] = Event::findOrFail($queryResults[$i]['event_id'])->tags()->pluck('tag');
		}

		return $queryResults;
	}
}
