<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Event;
use App\Evaluation;
use Carbon\Carbon;

class PromotorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAdvertised(Request $request)
    {
        $currentView = 'advertised';

        return view('promotor.advertised', compact('currentView'));
    }

    public function showCanceled ()
    {
        $currentView = 'canceled';

        return view('promotor.canceled', compact('currentView')); 
    }

    public function showFinished(Request $request)
    {
        $currentView = 'finished';

        return view('promotor.finished', compact('currentView'));
    }

    public function showTicketEventList()
    {
        $currentView = 'ticketmanagement';

        return view('promotor.tickets.event-list', compact('currentView'));
    }

    public function getAdvertised(Request $request)
    {
        return $this->packageEvents('1', null, $request->user()->id, $request->all());
    }


    public function getCanceled(Request $request)
    {
        return $this->packageEvents('2', '3', $request->user()->id, $request->all());
    }

    public function getFinished(Request $request)
    {
        return $this->packageEvents('4','5', $request->user()->id, $request->all());
    }

    public function getTicketEvents(Request $request)
    {
        return $this->packageEventsForTickets($request->user()->id, $request->all());
    }

    public function showPromotorStats (){
        $currentView = 'stats';

        $promotor = Auth::user();

        $promotorEventsCount = $promotor->events()->count();

        $counterFollowers = 0;
        $events = $promotor->events()->get();
        foreach ($events as $event) {
            $counterFollowers = $counterFollowers + $event->followers()->count();
        }

        $promotorStatsAvg['labels'] = array(/*'Nº Eventos', */'Média Visualizações', 'Média Partilhas', 'Média Seguidores');
        $promotorStatsAvg['text'] = "Total Eventos e Média por Evento";
        $promotorStatsAvg['data'] = array(
            /*$promotorEventsCount,*/
            $promotorEventsCount != 0 ? round($promotor->events()->sum('counter_views')/$promotorEventsCount) : 0,
            $promotorEventsCount != 0 ? round($promotor->events()->sum('counter_shares')/$promotorEventsCount) : 0,
            $promotorEventsCount != 0 ? round($counterFollowers/$promotorEventsCount) : 0
            );

        $promotorStatsTotal['labels'] = array('Total Visualizações', 'Total Partilhas', 'Total Seguidores');
        $promotorStatsTotal['text'] = "Totais de todos os Eventos";
        $promotorStatsTotal['data'] = array(
            $promotor->events()->sum('counter_views'),
            $promotor->events()->sum('counter_shares'),
            $counterFollowers
            );

        if($promotor->promotorEvals()->count()>0) {

            $promotorEval = round(Evaluation::where('promotor_id', $promotor->id)->sum('promotorEval')/Evaluation::where('promotor_id', $promotor->id)->count());

            $eventEval = round(Evaluation::where('promotor_id', $promotor->id)->sum('eventEval')/Evaluation::where('promotor_id', $promotor->id)->count());

        } else {
            $promotorEval = 0;
            $eventEval = 0;
        }

        $evaluations = Evaluation::where('promotor_id', $promotor->id)
                                 ->where('promotorOpinion', '!=', '')
                                 ->orderBy('created_at', 'desc')->paginate(10);

        return view('promotor.stats', compact('currentView', 'promotorEval', 'eventEval', 'promotorStatsAvg', 'promotorStatsTotal', 'promotorEventsCount', 'evaluations'));
    }

    public function packageEvents($firstVal, $secondVal, $user_id, $options)
    {
        $inArray = array($firstVal);

        if(!is_null($secondVal))
        {
            array_push($inArray, $secondVal);
        }

        $queryResults = Event::leftJoin('categories', 'events.category_id', 'categories.id')
        ->where('user_id', $user_id)
        ->whereIn('status', $inArray)
        ->orderBy('events.'.$options['orderByParam'], $options['orderByType'])
        ->select('events.*', 'categories.categoryName')
        ->paginate(10);

        for($i = 0; $i < sizeof($queryResults); $i ++)
        {
            $queryResults[$i]['tags'] = $queryResults[$i]->tags()->pluck('tag');
        }

        return $queryResults;
    }

    public function packageEventsForTickets($user_id, $options)
    {
        $queryResults = Event::leftJoin('categories', 'events.category_id', 'categories.id')
        ->where('user_id', $user_id)
        ->where('hasTickets', 1)
        ->where('ticketManagement', 'eventTracker')
        ->where('status', 1)
        ->orderBy('events.'.$options['orderByParam'], $options['orderByType'])
        ->select('events.*', 'categories.categoryName')
        ->paginate(10);


        for($i = 0; $i < sizeof($queryResults); $i ++)
        {
            $queryResults[$i]['tags'] = $queryResults[$i]->tags()->pluck('tag');
        }

        return $queryResults;
    }
}

