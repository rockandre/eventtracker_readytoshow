<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Event;
use App\User;
use App\Report;
use App\Category;

class AdministratorController extends Controller
{
    public function dashboard() 
    {
    	$currentView = 'dashboard';

        $date = Carbon::now();

        $newusers = User::whereDate('created_at', '=', $date->toDateString())->count();
        $newevents = Event::whereDate('created_at', '=', $date->toDateString())->count();
        $newreports = Report::whereDate('created_at', '=', $date->toDateString())->count();

        $eventsData = $this->getEventsData();
        $usersData = $this->getUsersData();
        $categoriesData = $this->getCategoriesData();
        $eventsThisYearData = $this->getEventsThisYearData();

        $total['users'] = User::count();

        $otherStatistics = $this->getOtherStatistics();

        return view('administrator.dashboard.statistics', compact('currentView', 'newusers', 'newevents', 'newreports', 'eventsData', 'usersData', 'categoriesData', 'eventsThisYearData', 'otherStatistics'));
    }

    public function getEventsData()
    {
        $date = Carbon::now();
        $labels = array();
        $data = array();

        for ($i=0; $i < 10; $i++) { 
            array_push($labels, $date->toDateString());
            array_push($data, Event::whereDate('created_at', $date->toDateString())->count());
            $date->subDay();
        }

        $labels = array_reverse($labels);
        $data = array_reverse($data);

        $eventsData['labels'] = $labels;
        $eventsData['data'] = $data;
        $eventsData['text'] = "Eventos criados por dia / Últimos 10 dias";

        return $eventsData;
    }

    public function getUsersData()
    {
        $date = Carbon::now();
        $labels = array();
        $data = array();

        for ($i=0; $i < 10; $i++) { 
            array_push($labels, $date->toDateString());
            array_push($data, User::whereDate('created_at', $date->toDateString())->count());
            $date->subDay();
        }

        $labels = array_reverse($labels);
        $data = array_reverse($data);

        $usersData['labels'] = $labels;
        $usersData['data'] = $data;
        $usersData['text'] = "Utilizadores Registados por dia / Últimos 10 dias";

        return $usersData;
    }

    public function getCategoriesData()
    {
        $labels = Category::select('categoryName')->where('id', '>', 1)->orderBy('counter', 'desc')->take(10)->pluck('categoryName');
        $data = Category::select('counter')->where('id', '>', 1)->orderBy('counter', 'desc')->take(10)->pluck('counter');

        $categoriesData['labels'] = $labels;
        $categoriesData['data'] = $data;
        $categoriesData['text'] = "Categorias com mais Eventos";

        return $categoriesData;
    }

    public function getEventsThisYearData()
    {
        $date = Carbon::now();
        $months = array (
            1=>'Janeiro',
            2=>'Fevereiro',
            3=>'Março',
            4=>'Abril',
            5=>'Maio',
            6=>'Junho',
            7=>'Julho',
            8=>'Agosto',
            9=>'Setembro',
            10=>'Outubro',
            11=>'Novembro',
            12=>'Dezembro');

        $date->subMonths(3);

        $labels = array();
        $data = array();

        for ($i=0; $i < 12; $i++) { 
            array_push($data, Event::whereMonth('initDate', $date->month)
                                   ->whereYear('initDate', $date->year)
                                   ->count());
            array_push($labels, $months[$date->month]);
            $date->addMonth();
        }

        $eventsThisYearData['labels'] = $labels;
        $eventsThisYearData['data'] = $data;
        $eventsThisYearData['text'] = " Número de Eventos Por Mês";

        return $eventsThisYearData;
    }

    public function getOtherStatistics()
    {
        $date = Carbon::now();

        $otherStatistics['events'] = Event::count();
        $otherStatistics['advertised'] = Event::whereDate('initDate', '>', $date->toDateString())
                                              ->where('status', 1)->count();
        $otherStatistics['ongoing'] = Event::whereDate('initDate', '<=', $date->toDateString())
                                              ->where('status', 1)->count();
        $otherStatistics['cancellations'] = Event::where('status', 2)->count();
        $otherStatistics['canceled'] = Event::where('status', 3)->count();
        $otherStatistics['finished'] = Event::where('status', 4)->count();

        $otherStatistics['users'] = User::count();
        $otherStatistics['blockedUsers'] = User::where('blocked', 1)->count();

        return $otherStatistics;
    }

    public function dashboardNewUsers()
    {
        $currentView = 'dashboard';

        $date = Carbon::now();

        $newusers = User::whereDate('created_at', $date->toDateString())->count();
        $newevents = Event::whereDate('created_at', $date->toDateString())->count();
        $newreports = Report::whereDate('created_at', $date->toDateString())->count();

        $users = User::whereDate('created_at', $date->toDateString())
                     ->orderBy('created_at', 'desc')->paginate(10);

        return view('administrator.dashboard.new-users', compact('currentView', 'newusers', 'newevents', 'newreports', 'users'));
    }

    public function dashboardNewEvents()
    {
        $currentView = 'dashboard';

        $date = Carbon::now();

        $newusers = User::whereDate('created_at', $date->toDateString())->count();
        $newevents = Event::whereDate('created_at', $date->toDateString())->count();
        $newreports = Report::whereDate('created_at', $date->toDateString())->count();

        $events = Event::whereDate('created_at', $date->toDateString())
                       ->orderBy('created_at', 'desc')->paginate(10);

        return view('administrator.dashboard.new-events', compact('currentView', 'newusers', 'newevents', 'newreports', 'events'));
    }

    public function dashboardNewReports()
    {
        $currentView = 'dashboard';

        $date = Carbon::now();

        $newusers = User::whereDate('created_at', $date->toDateString())->count();
        $newevents = Event::whereDate('created_at', $date->toDateString())->count();
        $newreports = Report::whereDate('created_at', $date->toDateString())->count();

        $reports = Report::whereDate('created_at', $date->toDateString())->paginate(10);

        return view('administrator.dashboard.new-reports', compact('currentView', 'newusers', 'newevents', 'newreports', 'reports'));
    }

    public function reportedEventsNotReviewed()
    {
    	$currentView = 'reportedNotReviewed';

        $reports = new Report;

        $queries = [];

        if (request()->has('searchText')) {
            $reports = $reports->leftJoin('events', 'reports.event_id', 'events.id')
            ->select('reports.*', 'events.name')
            ->where('reports.event_id', request('searchText'))
            ->orWhere('events.name', 'LIKE', '%'.request('searchText').'%');
            $queries['searchText'] = request('searchText');
        }

        $reports = $reports->where('reports.reviewed', false)->paginate(10)->appends($queries);

        return view('administrator.events.reports.list-not-reviewed', compact('currentView', 'reports'));
    }

    public function reportedEventsReviewed()
    {
        $currentView = 'reportedReviewed';

        $reports = new Report;

        $queries = [];

        if (request()->has('searchText')) {
            $reports = $reports->leftJoin('events', 'reports.event_id', 'events.id')
            ->select('reports.*', 'events.name')
            ->where('reports.event_id', request('searchText'))
            ->orWhere('events.name', 'LIKE', '%'.request('searchText').'%');
            $queries['searchText'] = request('searchText');
        }

        $reports = $reports->where('reports.reviewed', true)->paginate(10)->appends($queries);
        
        return view('administrator.events.reports.list-reviewed', compact('currentView', 'reports'));
    }

    public function hiddenEvents ()
    {
    	$currentView = 'hiddenEvents';

        $events = new Event;

        $queries = [];

        if (request()->has('searchText')) {
            $events = $events->where('id', request('searchText'))
            ->orWhere('name', 'LIKE', '%'.request('searchText').'%');
            $queries['searchText'] = request('searchText');
        }

        $events = $events->where('hidden', true)->paginate(10)->appends($queries);

        return view('administrator.events.hidden', compact('currentView', 'events'));
    }

    public function blockedUsers ()
    {
    	$currentView = 'blockedUsers';

    	$users = new User;

        $queries = [];

        if (request()->has('searchText')) {
            $users = $users->where('id', request('searchText'))
            ->orWhere('name', 'LIKE', '%'.request('searchText').'%');
            $queries['searchText'] = request('searchText');
        }

        $users = $users->where('blocked', true)->paginate(10)->appends($queries);

        return view('administrator.users.blocked', compact('currentView', 'users'));
    }

    public function listUsers ()
    {
    	$currentView = 'listUsers';
    	
        $users = new User;

        $queries = [];

        if (request()->has('searchText')) {
            $users = $users->where('id', request('searchText'))
            ->orWhere('name', 'LIKE', '%'.request('searchText').'%');
            $queries['searchText'] = request('searchText');
        }
        
        $users = $users->orderBy('is_admin', 'asc')->paginate(10)->appends($queries);

        return view('administrator.users.list', compact('currentView', 'users'));
    }

    public function newAdmin()
    {
    	$currentView = 'newAdmin';
    	
      return view('administrator.partials.new', compact('currentView'));
  }

  public function createNewAdmin(Request $request)
  {
    $errors = $this->validator($request->all())->validate();

    if(count($errors) > 0)
    {
        return render_view('administrator.partials.new', compact('errors'));
    }

    $user = $this->create($request->all());

    $user->save();

    return redirect()->back()->with('success', 'Administrador criado com sucesso!');
}

public function validator(array $data)
{
    $rules = array (
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|min:6|confirmed');

    $messages = array (
        'digits' => 'O :attribute tem de ter exatamente :digits carateres',
        'email.unique' => 'O email fornecido já se encontra em utilização',
        'password.confirmed' => 'As passwords não coincidem',
        'required' => 'O campo :attribute é de preenchimento obrigatório',
        'max' => 'O campo :attribute poderá ter no maximo :max carateres',
        'min' => 'O campo :attribute tem de ter pelo menos :min carateres');

    return Validator::make($data, $rules, $messages);
}

public function create(array $data)
{
    return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password']),
        'is_admin' => true,
        ]);
}

public function listEvents() 
{
    $currentView = 'listEvents';

    $events = new Event;

    $queries = [];

    if (request()->has('searchText')) {
        $events = $events->where('id', request('searchText'))
        ->orWhere('name', 'LIKE', '%'.request('searchText').'%');
        $queries['searchText'] = request('searchText');
    }

    $events = $events->paginate(10)->appends($queries);

    return view('administrator.events.list', compact('currentView', 'events'));
}

public function toogleEventVisibility($id) 
{
    $event = Event::findOrFail($id);

    $event->hidden = !$event->hidden;

    $event->save();

    return redirect()->back();
}

public function deleteEvent($id)
{
    $event = Event::findOrFail($id);

    $event->delete();

    return redirect()->back();
}

public function toogleBlock($id)
{
    $user = User::findOrFail($id);

    $user->blocked = !$user->blocked;

    $user->save();

    return redirect()->back();
}
}
