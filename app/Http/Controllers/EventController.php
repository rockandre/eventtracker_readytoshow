<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Category;
use App\District;
use App\Event;
use App\Evaluation;
use Intervention\Image\ImageManagerStatic as Image;
use App\Multimedia;
use App\SuggestedCategory;
use App\Tag;
use App\TicketSalesPoint;
use App\User;
use App\TicketType;

use Storage;
use App\Jobs\ProcessNotifyFollowersOnMessage;
use App\Jobs\ProcessNotifyFollowersOnChange;

class EventController extends Controller
{

    public function __construct() {

        $this->middleware('auth', ['except' => array('all', 'searchEvents', 'show', 'getFeaturedEvent')]);
        $this->middleware('editEvent', ['only' => array('edit', 'update')]);
    }

    public function all() 
    {
        $categories = Category::all();

        $districts = District::all();

        $tags = Tag::orderBy('counter', 'desc')->take(10)->get();

        return view('event.list', compact('categories', 'districts', 'tags'));
    }

    public function show($id) 
    {
        $event = Event::findOrFail($id);

        $event->counter_views = $event->counter_views+1;

        $event->save();

        $event['is_following'] = Auth::guest() ? false : (Auth::user()->followingEvents()
            ->where('event_id', $event->id)->count()) ? true : false;

        $date = Carbon::now();

        if(Auth::guest())
        {
            $event['inEval'] = $date->lt(Carbon::parse($event->finalEvalDay)) 
            && $date->gt(Carbon::parse($event->finishDate)) ? true : false;

            //In the future need to protect one visitor to not evaluate more than 1 time!
        }
        else {
            if(!Auth::user()->isAdmin()){
                $event['inEval'] = $date->lt(Carbon::parse($event->finalEvalDay)) 
                && $date->gt(Carbon::parse($event->finishDate)) 
                && ($event->user_id != Auth::user()->id )
                && $event->usersEvaluation()->where('user_id', Auth::user()->id)->count() == 0 ? true : false;
            }
        }

        $evaluations = Evaluation::where('event_id', $id)
        ->where('eventOpinion', '!=', '')
        ->orderBy('created_at', 'desc')->paginate(10);

        $shareButton = true;

        $shareOption['url'] = "http://eventtracker.app/event/".$event->id;
        $shareOption['description'] = $event->name;
        $shareOption['image'] = "http://eventtracker.app/images/event/".$event->image_id;

        return view('event.show', compact('event', 'evaluations', 'shareButton', 'shareOption'));
    }

    public function searchEvents(Request $request)
    {
        $query = Event::
        leftJoin('categories', 'events.category_id', 'categories.id')
        ->select('events.*', 'categories.categoryName')
        ->whereIn('events.status', ['1','2'])
        ->where('events.hidden', '=', '0')
        ->distinct()
        ->orderBy('events.'.$request['orderByParam'], $request['orderByType']);

        if(!empty($request['searchText']))
        {
            $query->where('events.name', 'LIKE', '%'.$request['searchText'].'%');
        }

        if(!empty($request['categoriesToSearch']))
        {
            $query->whereIn('events.category_id', $request['categoriesToSearch']);
        }

        if(!empty($request['tagsToSearch']))
        {
            $eventsIds = DB::table('event_tag')->whereIn('tag_id', $request['tagsToSearch'])->pluck('event_id');
            $query->whereIn('events.id', $eventsIds);
        }

        if($request['district_id'] > 1)
        {
            $query->where('events.district_id', $request['district_id']);
        }

        if((!empty($request['dateInit']) || $request['dateInit']) 
            && (!empty($request['dateFinish']) || $request['dateFinish']))
        {
            $dateInit = Carbon::parse($request['dateInit']);
            $dateFinish = Carbon::parse($request['dateFinish']);
            $query->where([
                ['events.initDate', '>',  $dateInit],
                ['events.initDate', '<', $dateFinish],
                ]);
        } else {
            if(!empty($request['dateInit']) || $request['dateInit'])
            {
                $dateInit = Carbon::parse($request['dateInit']);

                $query->whereDate('events.initDate', '>', $dateInit);

            }

            if(!empty($request['dateFinish']) || $request['dateFinish'])
            {
                $dateFinish = Carbon::parse($request['dateFinish']);

                $query->whereDate('events.initDate', '<', $dateFinish);
            }
        }
        
        $events = $query->paginate(10);

        for($i = 0; $i < sizeof($events); $i ++)
        {
            $events[$i]['tags'] = $events[$i]->tags()->pluck('tag');
        }

        return $events;
    }  

    public function getFeaturedEvent()
    {
        $event = Event::select('id', 'name', 'initDate', 'district_id', 'image_id')->where('status', 1)->where('hidden', 0)->inRandomOrder()->first();


        $event['district_id'] = District::select('name')->findOrFail($event['district_id']);

        $event['image'] = Image::make(Storage::disk('local')->get('public/event_list/' . $event['image_id']))->encode('data-url')->encoded;



        return $event;
    }

    /** Create an Event  **/

    public function create() 
    {
        $currentView = 'create';

        $categories = Category::all();

        $districts = District::all();

        return view('event.create', compact('currentView', 'categories', 'districts'));

    }

    public function store (Request $request) 
    {

        $this->validator($request->all())->validate();

        $event = new Event();
        $event->user_id = $request->user()->id;

        $this->saveEvent($request->all(), $event);

        $currentView = 'ongoing';

        return ['redirect' => url('/promotorView/advertised')];

    }



    /**  Update an Event **/

    public function edit ($id)
    {
        $currentView = '';

        $categories = Category::all();

        $districts = District::all();

        $event = Event::findOrFail($id);

        $event['multimedia'] = $event->multimediaResources()->pluck('url', 'name');
        $event['tags'] = $event->tags()->pluck('tag');
        if($event['hasTickets'])
        {
            $event['ticketSales'] = $event->ticketSalesPoints()->pluck('url');
        }

        if($event->ticketManagement == 'eventTracker')
        {
            $event['tickets'] = $event->ticketsTypes()->select('name', 'quantity', 'price', 'ageRestrictionMin', 'ageRestrictionMax', 'is_standard')->orderBy('created_at', 'asc')->get();
        }

        return view('event.edit', compact('currentView', 'categories', 'districts', 'event' ));
    }


    public function update(Request $request, $id)
    {
        $this->validator($request->all())->validate();

        $event = Event::findOrFail($id);

        Multimedia::where('event_id', $id)->delete();
        SuggestedCategory::where('event_id', $id)->delete();
        TicketSalesPoint::where('event_id', $id)->delete();
        $event->tags()->detach();

        $this->saveEvent($request->all(), $event);

        return ['redirect' => url('/promotorView/advertised')];

    }

    public function sendChangeEventMessage($id)
    {
        $event = Event::findOrFail($id);

        $users = $event->followers()->get();

        $job = (new ProcessNotifyFollowersOnChange($users, $event));

        dispatch($job);

        return ['redirect' => url('/promotorView/advertised')];
    }


    /*** Helper Functions ***/

    private function saveEvent($data, $event)
    {    

        $event->name = $data['name'];
        $event->initDate = Carbon::parse($data['initDate']);
        $event->finishDate = Carbon::parse($data['finishDate']);
        $event->description = $data['description'];
        $event->spaceName = $data['spaceName'];
        $event->address = $data['address'];
        $event->lat = $data['location']['lat'];
        $event->lng = $data['location']['lng'];
        $event->hasTickets = $data['hasTickets'];
        $event->ticketsForSale = isset($data['ticketsForSale']) ? $data['ticketsForSale'] : null;
        $event->ticketManagement = isset($data['ticketManagement']) ? $data['ticketManagement'] : null ;
        $event->ticketSaleDescription = isset($data['ticketSaleDescription']) ? $data['ticketSaleDescription'] : null;
        $event->evalType = $data['evalType'];
        $event->evalTime = $data['evalTime'];
        $event->finalEvalDay = $this->getFinalEvalDay($data['finishDate'], $data['evalTime']);
        $event->district_id = $data['district'];
        $event->category_id = !$data['hasSuggestedCategory'] ? $data['category'] : null;

        $event->finalEvalDay->second = 0;
        $event->initDate->second = 0;
        $event->finishDate->second = 0;
        
        $event->save();

        if($data['hasSuggestedCategory']) {
            $this->newSuggestedCategory($data['newCategory'], $event->id);
        } else {
            $category = Category::findOrFail($data['category']);
            $category->counter++;

            $category->save();
        }

        $this->defineEventTags($data['tags'], $event->id);

        if(!empty($data['multimedia']))
            $this->newMultimediaResources($data['multimedia'], $event->id);

        if($data['ticketManagement'] == 'external')
        {   
            $this->newTicketSalesPoints($data['ticketSales'], $event->id);
        }
        else{
            $this->storeTicketTypes(isset($data['standardTicket']) ? $data['standardTicket'] : null, $data['tickets'], $event->id);
        }

        if(isset($data['image']))
        {
            $event->image_id = $this->newEventImage($data['image'], $event->id);
            $event->save();
        }

    }

    private function storeTicketTypes($standardTicket, array $tickets, $event_id)
    {

        if(!is_null($standardTicket))
        {   
            $hasAgeRestriction = isset($standardTicket['ageRestrictionMin']) && isset($standardTicket['ageRestrictionMax']);


            TicketType::create(['event_id' => $event_id, 
                                'name' => $standardTicket['name'], 
                                'quantity' => $standardTicket['quantity'], 
                                'price' => $standardTicket['price'], 
                                'ageRestrictionMin' => $hasAgeRestriction ? $standardTicket['ageRestrictionMin'] : null, 
                                'ageRestrictionMax' => $hasAgeRestriction ? $standardTicket['ageRestrictionMax'] : null,
                                'is_standard' => true
                            ]);
        }

        foreach ($tickets as $key => $value) {

            $hasAgeRestriction = isset($value['ageRestrictionMin']) && isset($value['ageRestrictionMax']);

            TicketType::create(['event_id' => $event_id, 
                                'name' => $value['name'], 
                                'quantity' => $value['quantity'], 
                                'price' => $value['price'], 
                                'ageRestrictionMin' => $hasAgeRestriction ? $value['ageRestrictionMin'] : null,
                                'ageRestrictionMax' => $hasAgeRestriction ? $value['ageRestrictionMax'] : null,
                                'is_standard' => false
                                ]);
        }

    }

    private function getFinalEvalDay($finishDate, $evalTime)
    {  

        $finishDate = Carbon::parse($finishDate);

        if($evalTime != '1m') {

            $finishDate->addDays($evalTime);  

        } else {

            $finishDate->addMonth();
        }

        return $finishDate;

    }

    private function newSuggestedCategory ($name, $event_id)
    {
        $newSuggestedCategory = SuggestedCategory::create(['name' => $name, 'event_id' => $event_id]);
        $newSuggestedCategory->save();
    }

    private function newMultimediaResources($multimedia, $event_id)
    {
        foreach ($multimedia as $key => $value) {

            if(!empty($value['name']) && !empty($value['url']))
            {   
                Multimedia::create(['name' => $value['name'], 'url' => $value['url'], 'event_id' => $event_id]);
            }
        }
    }

    private function newTicketSalesPoints($ticketSales, $event_id)
    {
        foreach ($ticketSales as $key => $value) {
            if(!empty($value))
            {   
                TicketSalesPoint::create(['url' =>$value, 'event_id' => $event_id]);
            }
        }
    }

    private function defineEventTags ($tags, $event_id)
    {
        foreach($tags as $tag)
        {
            if($tag[0] == '#')
            {
                $tag = substr($tag, 1);
            }

            $queryTag = Tag::where('search_tag', strtoupper($tag))->first();

            if(is_null($queryTag))
            {
                $createdTag = Tag::create(['tag' => $tag, 'search_tag' => strtoupper($tag)]);

                $createdTag->events()->attach($event_id);
            }
            else{
                $queryTag->counter ++;
                $queryTag->save();
                $queryTag->events()->attach($event_id);
            }
        }
    }


    private function newEventImage ($image, $event_id)
    {

        $fileName = $event_id . '_' . time() . '.png';

        $image = Image::make($image);
        $image->heighten(960);
        $image->save(storage_path('/app/public/event/' . $fileName), 100);
        $image->heighten(200);
        $image->save(storage_path('/app/public/event_list/' . $fileName), 100);

        return $fileName;

    }  



    /*** VALIDATION SECTION ***/

    /**
    * Get a validator for an incoming registration request.
    *
    * @param  array  $data
    * @return \Illuminate\Contracts\Validation\Validator
    */
    protected function validator(array $event)
    {   

        return Validator::make($event, $this->rules($event), $this->messages($event));

    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules($event)
    {       

        $rules = array (
            'name' => 'required|max:255',
            'initDate' => 'required|date',
            'finishDate' => 'required|date',
            'description' => 'required|max:65000',
            'district' => 'not_in:1',
            'hasTickets' => 'required',
            'location' => 'required',
            'tags' => 'required',
            'ticketSales.*' => 'nullable|max:2500', 
            'evalType' => 'required',
            'evalTime' => 'required',
            );


        if(isset($event['hasTickets']) && $event['hasTickets'])
        {
            $rules['ticketManagement'] = 'required';

            if($event['ticketManagement'] == 'external')
            {
                $rules['ticketsForSale'] = 'required|numeric|max:250000';
            }

        }


        if($event['category'] == 1)
        {
            $rules['category'] = 'not_in:1';
        }

        if($event['hasSuggestedCategory'])
        {
            $rules['newCategory'] = 'required|max:50';
        }

        if(isset($event['multimedia']))
        {   
            foreach ($event['multimedia'] as $key => $value) {

                if(empty($value['name']) && !empty($value['url']))
                {
                    $rules['multimedia.'.$key.'.name'] = 'required|max:150';
                }

                if(empty($value['url']) && !empty($value['name']))
                {
                    $rules['multimedia.'.$key.'.url'] = 'required|url|max:2500';
                }
            }
        }

        if($event['ticketManagement'] == 'eventTracker')
        {


            if(isset($event['tickets']))
            {
                foreach ($event['tickets'] as $key => $value) {

                    $rules['tickets.'.$key.'.name'] = 'required|max:125';
                    $rules['tickets.'.$key.'.price'] = 'required|numeric|max:10000|min:0';
                    $rules['tickets.'.$key.'.quantity'] = 'required|numeric|max:250000|min:1';


                    if(!empty($value['ageRestrictionMin']) ||  !empty($value['ageRestrictionMax']))
                    {   

                        $rules['tickets.'.$key.'.ageRestrictionMin'] = 'required|numeric|min:0|max:105';

                        $rules['tickets.'.$key.'.ageRestrictionMax'] = 'required|numeric|max:105|min:'. (!empty($value['ageRestrictionMin']) ? $value['ageRestrictionMin'] : '0');
                    }

                }
            }

            if(isset($event['standardTicket']))
            {
                $rules['standardTicket.name'] = 'required|max:125';
                $rules['standardTicket.price'] = 'required|numeric|max:10000|min:0';
                $rules['standardTicket.quantity'] = 'required|numeric|max:250000|min:1';

                if(!empty($event['standardTicket']['ageRestrictionMin']) ||  !empty($event['standardTicket']['ageRestrictionMax']))
                {
                    $rules['standardTicket.ageRestrictionMin'] = 'required|numeric|min:0|max:105';
                    $rules['standardTicket.ageRestrictionMax'] = 'required|numeric|max:105|min:'. (!empty($event['standardTicket']['ageRestrictionMin']) ? $event['standardTicket']['ageRestrictionMin'] : '0');
                }
            }
        }

        return $rules;
    }


        /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
        public function messages($event)
        {

            $messages = array (

                'name.required' => 'É necessário definir o nome do evento',

                'name.max' => 'O nome do evento não poderá ter mais de :max carateres',

                'initDate.required' => 'Cada evento deverá ter uma data de inicio',

                'finishDate.required' => 'Cada evento deverá ter uma data de fim',

                'date' => 'A data fornecida deverá estar num formato válido',

                'description.required' => 'É necessário fornecer uma descrição para o evento',

                'district.not_in' => 'É necessário definir o distrito onde o evento se irá realizar',

                'hasTickets.required' => 'É necessário definir se o evento terá bilhetes', 

                'ticketsForSale.required' => 'É necessário definir a quantidade maxima de bilhetes que estarão à venda',

                'category.not_in' => 'É necessário definir a categoria em que evento se enquadra',

                'location.required' => 'Por favor defina a localização do seu evento no Mapa Acima', 

                'newCategory.required' => 'Sugira uma nova categoria para o seu evento',

                'newCategory.max' => 'A sugestão fornecida deverá ter menos de 50 carateres',

                'ticketManagement.required' => 'É necessário definir que serviço fará a gestão da bilheteira',

                'evalTime.required' => 'É necessário definir o intervalo de tempo para avaliação do evento',

                'evalType.required' => 'É necessário definir as condições de avaliação de um evento',

                'image' =>'As imagens fornecidas têm de ter um dos seguintes formatos: jpeg, png ou bmp',

                'tags.required' => 'Por favor especifique pelo menos uma etiqueta aplicavel ao evento',

                'ticketSales.*.max' => 'A hiperligação não pode ter mais de :max carateres',

                'ticketSales.*.url' => 'A hiperligação fornecida deverá ter um formato válido',

                'required_if.hasTickets' => 'Por favor defina se o evento terá bilhetes',

                'tickets.*.name.required' => 'É necessário definir o nome do bilhete',

                'tickets.*.price.required' => 'É necessário definir o preço do bilhete',

                'tickets.*.quantity.required' => 'É necessário definir a quantidade de bilhetes disponivel para venda',

                'tickets.*.ageRestrictionMin.required' => 'É necessário definir ambos os campos de restrição de idade',

                'tickets.*.ageRestrictionMax.required' => 'É necessário definir ambos os campos de restrição de idade',

                'tickets.*.name.max' => 'O nome do bilhete não poderá conter mais de :max carateres',

                'tickets.*.price.max' => 'O preço do bilhete não deverá ser maior que :max €',

                'tickets.*.price.min' => 'O preço do bilhete deverá ser maior que :min €',

                'tickets.*.quantity.max' => 'Não poderão ser disponibilizados mais de :max bilhetes de um determinado tipo',

                'tickets.*.quantity.min' => 'Deverá disponibilizar pelo menos :min bilhetes de um determinado tipo',

                'tickets.*.ageRestrictionMax.min' => 'A maior idade de restrição deverá ser de pelo menos :min anos',

                'tickets.*.ageRestrictionMax.max' => 'A maior idade de restrição não deverá ultrapassar os :max anos',

                'tickets.*.ageRestrictionMin.min' => 'A menor idade de restrição deverá ser de pelo menos :min anos',

                'tickets.*.ageRestrictionMin.max' => 'A menor idade de restrição não deverá ultrapassar os :max anos',

                'standardTicket.name.required' => 'É necessário definir o nome do bilhete',

                'standardTicket.price.required' => 'É necessário definir o preço do bilhete',

                'standardTicket.quantity.required' => 'É necessário definir a quantidade de bilhetes disponivel para venda',

                'standardTicket.ageRestrictionMin.required' => 'É necessário definir ambos os campos de restrição de idade',

                'standardTicket.ageRestrictionMax.required' => 'É necessário definir ambos os campos de restrição de idade',

                'standardTicket.name.max' => 'O nome do bilhete não poderá conter mais de :max carateres',

                'standardTicket.price.max' => 'O preço do bilhete não deverá ser maior que :max €',

                'standardTicket.price.min' => 'O preço do bilhete deverá ser maior que :min €',

                'standardTicket.quantity.max' => 'Não poderão ser disponibilizados mais de :max bilhetes de um determinado tipo',

                'standardTicket.quantity.min' => 'Deverá disponibilizar pelo menos :min bilhetes de um determinado tipo',

                'standardTicket.ageRestrictionMax.min' => 'A maior idade de restrição deverá ser pelo menos :min',

                'standardTicket.ageRestrictionMax.max' => 'A maior idade de restrição deverá ultrapassar os :max anos',

                'standardTicket.ageRestrictionMix.min' => 'A menor idade de restrição deverá ser pelo menos :min',

                'standardTicket.ageRestrictionMix.max' => 'A menor idade de restrição não deverá ultrapassar os :max anos',


                );

        if(isset($event['multimedia']))
        {   
            foreach ($event['multimedia'] as $key => $value) {

                if(empty($value['name']) && !empty($value['url']))
                {
                    $messages['multimedia.'.$key.'.name.required'] = 'Por favor defina o nome do recurso multimedia fornecido';
                    $messages['multimedia.'.$key.'.name.max'] = 'O nome do recurso multimedia não deve conter mais de :max carateres';
                }

                if(empty($value['url']) && !empty($value['name']))
                {

                    $messages['multimedia.'.$key.'.url.required'] = 'Por favor defina a hiperligação para o recurso multimedia';

                    $messages['multimedia.'.$key.'.url.max'] = 'A Hiperligação do recurso multimedia não deve conter mais de :max carateres';
                    $messages['multimedia.'.$key.'.url.url'] = 'A Hiperligação fornecida deverá ser válida';
                }
            }
        }

    return $messages;
    }

}
