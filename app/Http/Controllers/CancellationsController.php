<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Cancellation;
use App\Event;
use App\Mail\CanceledFollowedEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Jobs\ProcessEventCancellation;
use App\Jobs\ProcessNotifyFollowersOnCancel;

class CancellationsController extends Controller
{

    public function show($id)
    {
        $currentView = '';

        $event = Event::findOrFail($id);

        $event['categoryName'] = $event->category()->pluck('categoryName')[0];

        $event['tags'] = $event->tags()->pluck('tag');

        return view('event.cancel', compact('currentView', 'event'));
    }

    public function store (Request $request, $id)
    {   
        $event = Event::findOrFail($id);

        if($event)
        {
            $finalCancellationDate = $this->getFinalCancellationDate($event->finishDate);

            $cancellation = Cancellation::create(['reason' => $request->get('reason'), 
                'final_cancel_day' => $finalCancellationDate,
                'event_id' => $event->id]);

            $event->status = 2;
            $event->save();

            
            $jobStatus = (new ProcessEventCancellation($event))->delay($finalCancellationDate);

            dispatch($jobStatus);

            /*
            $this->sendCanceledEventMessage($event, $cancellation->reason);
            */
        }

        return ['redirect' => url('/promotorView/canceled')];
    }

    private function getFinalCancellationDate($finishDate)
    {
        $finalCancellationDate = Carbon::now()->addWeeks(2);

        var_dump ($finalCancellationDate);

        if($finalCancellationDate > $finishDate)
        {
            return $finishDate;
        }
        else{
            return $finalCancellationDate;

        }
    } 

    /* Canceled email */
    public function sendCanceledEventMessage($event, $message)
    {
        $users = $event->followers()->get();

        $jobSendMessage = (new ProcessNotifyFollowersOnCancel($users, $event, $message));

        dispatch($jobSendMessage);
    }

    /*
    * @param  array  $data
    * @return \Illuminate\Contracts\Validation\Validator
    */

    protected function validator(array $request)
    {   
        return Validator::make($request, $this->rules($request), $this->messages($request));
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules($request)
    {       
        $rules = array (
            'reason' => 'required|between:10,500',
            );
        return $rules;
    }


    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages($request)
    {
        $messages = array (
            'reason.required' => 'É necessário definir a razão de cancelamento do evento!',
            'reason.between' => 'A razão fornecida deverá estar compreendida entre :min e :max'
            );
        return $messages;
    }
}
