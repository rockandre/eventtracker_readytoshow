<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;
use App\Ticket;
use App\TicketType;
use Carbon\Carbon;

class TicketController extends Controller
{
    public function show(Request $request, $id)
    {

        $event = Event::where('events.id', $id)->leftJoin('categories', 'events.category_id', 'categories.id')
        ->select('events.*', 'categories.categoryName')->first();

        $event['initDate'] = $event->getDate($event['initDate']);
        $event['finishDate'] = $event->getDate($event['finishDate']);
        $event['tags'] = $event->tags()->pluck('tag');

        $ticketTypeIds = $event->ticketTypes()->pluck('id');

        $event['tickets'] = $event->ticketTypes()->select('id', 'name', 'price', 'quantity', 'ageRestrictionMin', 'ageRestrictionMax', 'is_standard')->orderBy('price', 'asc')->get();

        $event['tickets'] = $this->checkRemaningTicketQuantities($event['tickets'], $ticketTypeIds);

        return view('tickets.sales', compact('event'));
    }

    private function checkRemaningTicketQuantities($tickets, $ticketTypeIds)
    {   

        $counters = DB::table('tickets')
        ->whereIn('ticket_type_id', $ticketTypeIds)
        ->select('ticket_type_id', DB::raw('count(*) as total'))
        ->groupBy('ticket_type_id')
        ->get();

        foreach ($tickets as $key => $value) {


            $found = false;
            for ($i=0; $i < sizeof($counters); $i++) { 
                if($value['id'] == $counters[$i]->ticket_type_id)
                {
                    $tickets[$key]['remaining'] = $value['quantity'] - $counters[$i]->total;
                    $found = true;
                }

                if(!$found)
                {
                    $tickets[$key]['remaining'] = $value['quantity'];
                }           
            }
        }

        return $tickets;
    }


    public function eventTickets($id)
    {
        $event = Event::findOrFail($id);

        $currentView = 'ticketmanagement';

        $ticketsData = $this->getGraphInfo($event);

        return view('promotor.tickets.event-tickets', compact('event', 'currentView', 'ticketsData'));
    }


    public function getGraphInfo($event)
    {
        $date = Carbon::now();
        $labels = array();
        $data = array();

        while ($date >= $event->created_at) {
            array_push($labels, $date->toDateString());
            array_push($data, Ticket::leftJoin('ticket_types', 'tickets.ticket_type_id', 'ticket_types.id')
                ->where('ticket_types.event_id', $event->id)
                ->whereDate('tickets.created_at', $date->toDateString())->count());
            $date->subDay();
        }

        $ticketsData['labels'] = $labels;
        $ticketsData['data'] = $data;
        $ticketsData['text'] = "Dias de compras de bilhetes";

        return $ticketsData;
    }

    public function eventEntrances($id)
    {
        $event = Event::findOrFail($id);

        $currentView = 'ticketmanagement';

        $ticketsData = array();

        array_push($ticketsData, Ticket::leftJoin('ticket_types', 'tickets.ticket_type_id', 'ticket_types.id')
            ->where('ticket_types.event_id', $event->id)
            ->select('tickets.*', 'ticket_types.name', 'ticket_types.event_id','ticket_types.price')
            ->get());


        return view('promotor.tickets.event-entrances', compact('event', 'currentView', 'ticketsData'));
    }



    public function searchTickets(Request $request, $id)
    {
        $query = Ticket::leftJoin('ticket_types', 'tickets.ticket_type_id', 'ticket_types.id')
        ->where('ticket_types.event_id', $id)
        ->select('tickets.*', 'ticket_types.name', 'ticket_types.event_id','ticket_types.price');
        >>>>>>> 4bbcf8cf229fd57e7fe816289058a7a86dc81998

        if(!empty($request->input('searchWord')))
        {
            $query->where('tickets.identification_number', 'LIKE', '%'.$request['searchWord'].'%');
        }

        $tickets = $query->get();

        return $tickets;

    }

    public function giveEntry($id)
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->status = 2;

        $ticket->save();
    }

    public function rejectEntry($id)
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->status = 3;

        $ticket->save();
    }

    public function revertOperation($id)
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->status = 1;

        $ticket->save();
    }
}

