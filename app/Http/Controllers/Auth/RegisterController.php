<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\SocialAccount;
use App\FacebookTempAccount;
use App\SocialAccountService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\CreateAccountToMergeWithSocialAccount;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        $rules = array (
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed');

        $messages = array (
            'email.unique' => 'O email fornecido já se encontra em utilização',
            'password.confirmed' => 'As passwords fornecidas não coincidem',
            'name.required' => 'O campo nome é de preenchimento obrigatório',
            'required' => 'O campo :attribute é de preenchimento obrigatório',
            'email' => 'O email fornecido terá de ter um formato válido',
            'max' => 'O campo :attribute poderá ter no maximo :max carateres',
            'min' => 'O campo :attribute tem de ter pelo menos :min carateres'
            );

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
            ]);
    }

    public function confirmation($token)
    {
        $user = User::where('email_token', $token)->first();

        if(!is_null($user)) {
            $user->confirmed = 1;
            $user->email_token = 'ACTIVATED';
            $user->save();

            return redirect(route('login'))->with('messageS', 'Conta Ativada com sucesso!');
        }
        
        return redirect(route('login'))->with('messageD', 'Algo não correu bem por favor entre em contacto connosco!');
    }

    public function createFacebookAccount($token)
    {
        $tempUser = FacebookTempAccount::where('email_token', $token)->first();

        $service = new SocialAccountService();

        if(!is_null($tempUser)) {
            $user = User::create([
                'email' => $tempUser->given_email,
                'name' => $tempUser->name,
                'social_account' => 1
                ]);

            $user->confirmed = 1;
            $user->email_token = 'ACTIVATED';

            $user->image_id = $service->getFacebookAccountImage($tempUser->provider_user_id, $user->id);
            $user->save();

            $account = new SocialAccount([
                'user_id' => $user->id,
                'provider_user_id' => $tempUser->provider_user_id,
                'provider' => 'facebook'
                ]);

            $account->save();

            $tempUser->delete();

            return redirect(route('login'))->with('messages', 'Email associado com sucesso!');
        }

        return redirect(route('login'))->with('messageD', 'Algo não correu bem por favor entre em contacto connosco!');
    }

    public function createAccountThroughMail(Request $request) 
    {
        $user = User::where('email', $request->get('email'))->first();

        $user->email_token = str_random(25);

        $user->password = bcrypt($request->get('password'));

        $user->save();

        Mail::to($request->get('email'))->send(new CreateAccountToMergeWithSocialAccount($request->get('email')));
    }

    public function saveRegistedPassword($token)
    {
        $user = User::where('email_token', $token)->first();

        $user->email_token = 'ACTIVATED';

        $user->save();

        return redirect(route('login'))->with('messageS', 'Contas associadas com sucesso!');
    }
}
