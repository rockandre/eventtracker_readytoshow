<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\FacebookTempAccount;

class FacebookAccountWithoutMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FacebookTempAccount $user)
    {
        $this->email = $user->given_email;
        $this->url = url('/users/facebook/accountCreation', $user->email_token); 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Atribuição de Email')->markdown('emails.facebook-without-mail');
    }
}
