<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookTempAccount extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'facebook_temp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'provider_user_id', 'name', 'given_email'
    ];


}
