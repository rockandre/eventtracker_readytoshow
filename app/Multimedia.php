<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    public $timestamps = false;

    protected $table = 'multimedia';

    /* Quick notes on event->status:
    *	1 - Status = Online
    *	2 - Status = In Eval
    *	3 - Status = Finished
    *	4 - Status = Canceled
    */

    protected $fillable = array('name', 'url', 'event_id');


    public function event(){

       return $this->belongsTo('App\Event');
    
    }
}

