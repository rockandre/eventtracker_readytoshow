"use strict";

$(document).ready(function ($) {

    function initAlertify(){
        alertify.defaults.transition = "slide";
        alertify.defaults.theme.ok = "button is-medium is-success";
        alertify.defaults.theme.cancel = "button is-medium is-danger";
        alertify.set('notifier', 'position', 'top-right');
        alertify.defaults.maintainFocus = false;
    };

    initAlertify();

    $('.navbar-toggle-sidebar').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);
    });

    $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');
        $('.search-input').focus();
    });

    $('.hideEvent').click(function() {
        var button = $(this).prop('name');
        alertify.confirm('Ocultar Evento', 'Deseja mesmo ocultar este Evento?', function(){ 
            $('#hideEvent-form-'+button).submit();
        }
        , function(){ 

        }).set('labels', {ok : 'Sim', cancel : 'Não'});

    });

    $('.deleteEvent').click(function() {
        var button = $(this).prop('name');
        alertify.confirm('Eliminar Evento', 'Deseja mesmo eliminar este Evento?', function(){ 
            $('#deleteEvent-form-'+button).submit();
        }
        , function(){ 
            
        }).set('labels', {ok : 'Sim', cancel : 'Não'});

    });

     $('.blockUser').click(function() {
        var button = $(this).prop('name');
        alertify.confirm('Bloquear Utilizador', 'Deseja mesmo bloquear este Utilizador?', function(){ 
            $('#blockUser-form-'+button).submit();
        }
        , function(){ 
            
        }).set('labels', {ok : 'Sim', cancel : 'Não'});

    });
});
