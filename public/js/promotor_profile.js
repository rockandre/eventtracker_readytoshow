"use strict";

$(document).ready(function ($) {

	function loadTextAreaAutoSize(){
		autosize($('textarea'));
	};

	loadTextAreaAutoSize();
});