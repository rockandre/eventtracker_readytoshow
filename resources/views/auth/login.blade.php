@extends('layouts.app')

@section('content')

<link href="{{asset('css/login.css')}}" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<div id="authWrapper" class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            @if($messageD = Session::get('messageD'))
            <div align="center" class="alert alert-danger">
                <b>{{$messageD}}</b>
            </div>
            @endif
            @if($messageS = Session::get('messageS'))
            <div align="center" class="alert alert-success">
                <b>{{$messageS}}</b>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 align="center" class="tittle is-4"> Autenticação</h4>

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-xs-12 col-sm-4 col-sm-offset-1 control-label">
                                Endereço E-mail:</label>

                                <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-xs-12 col-sm-3 col-sm-offset-1 control-label">Password</label>

                                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <a class="col-xs-12 btn btn-link" href="{{ route('password.request') }}">Recuperar Password</a>
                            </div>

                            <div class="form-group">


                                <div class="col-md-12 col-xs-12">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <button id="submit" type="submit" class="button is-primary is-medium">Entrar</button>
                                        <a id="register" class="button is-info is-medium " href="{{ url('/register')}}">Criar Nova Conta</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <h4 align="center" id="socialh4" class="tittle is-4"> Autenticação Com Redes Sociais</h4>

                        <div class="form-group col-xs-10 col-xs-offset-1">
                            <a id="facebookLogin" href="{{ url('/redirect/facebook')}}" class="btn socialBtn">
                                <i class="fa fa-facebook-square fa-4" aria-hidden="true"></i>    Facebook</a>

                                <a id="googleLogin" href="{{ url('/redirect/google')}}" class="btn socialBtn">
                                    <i class="fa fa-google fa-4" aria-hidden="true"></i> Google</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection