@extends('layouts.app')

@section('content')

<link href="{{asset('css/login.css')}}" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if($error = Session::get('error'))
            <div align="center" class="alert alert-danger">
                <b>{{$error}}</b>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Introduzir o email</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register/setFacebookEmail', $id) }}">
                        {{ csrf_field() }}
                        <p class="alignCenter">
                            Reparámos que não tem um email associado à sua rede social. <br>Para o registo é necessário essa informação!
                        </p>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Endereço E-mail:</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" value="">

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary col-md-4">Continuar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
