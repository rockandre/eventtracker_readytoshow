<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Event Tracker') }}</title>

    <!-- Styles -->
    <link href="{{ URL::asset('/css/app.css') }}" rel="stylesheet">
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>

    <script src="{{ URL::asset('/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>

    <meta property="og:url" content="eventtracker.app" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="EventTracker" />
    <meta property="og:description" content="EventTracker" />

    @include('partials.navbar')
</head>
<body style="padding-top: 65px">

    @if(isset($shareButton))
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.9&appId=1704231253202692";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    @endif

    <div>
        <div class="container">
            @yield('content')
        </div>
        @yield('admin')
    </div>

</body>

</html>
