@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/evaluate-event.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Avaliar Evento</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/event/evaluate', $event->id) }}">
						{{ csrf_field() }}

						<div class="form-group paddingTop">
							<label class="col-md-4 control-label">Nome do Evento</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Nome do Promotor</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->user->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Distrito</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->district->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data de Inicio</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->initDate }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data de Fim</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->finishDate }}</label>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label id="evaluation" for="evaluation" class="col-md-4 control-label">Avaliação do Evento</label>
							<div class="col-md-6">
								<fieldset class="rating" id="ratingE">
									<input type="radio" id="star5E" name="ratingE" value="5" {{old('ratingE') == 5 ? 'checked' : ''}}/><label class = "full" for="star5E" title="Muito Bom - 5 estrelas"></label>
									<input type="radio" id="star4E" name="ratingE" value="4" {{old('ratingE') == 4 ? 'checked' : ''}}/><label class = "full" for="star4E" title="Bom - 4 estrelas"></label>
									<input type="radio" id="star3E" name="ratingE" value="3" {{old('ratingE') == 3 ? 'checked' : ''}}/><label class = "full" for="star3E" title="Razoável - 3 estrela"></label>
									<input type="radio" id="star2E" name="ratingE" value="2" {{old('ratingE') == 2 ? 'checked' : ''}}/><label class = "full" for="star2E" title="Mau - 2 estrela"></label>
									<input type="radio" id="star1E" name="ratingE" value="1" {{old('ratingE') == 1 ? 'checked' : ''}}/><label class = "full" for="star1E" title="Muito Mau - 1 estrela"></label>
								</fieldset>
							</div>

							@if ($errors->has('ratingE'))
							<div class="col-xs-10 col-xs-offset-1">
								<span class="error-span">
									<strong>{{ $errors->first('ratingE') }}</strong>
								</span>
							</div>
							@endif
						</div>
						<div class="form-group">
							<label for="opinion" class="col-md-4 control-label">Opinião sobre o Evento</label>

							<div class="col-md-7">
								<textarea id="opinion" class="form-control textareaSize" name="eventOpinion" placeholder="Deixe aqui a sua opinião sobre o Evento!" maxlength="1000">{{old('eventOpinion')}}</textarea>

							</div>
							@if ($errors->has('eventOpinion'))
							<div class="col-xs-10 col-xs-offset-1">
								<span class="error-span">
									<strong>{{ $errors->first('eventOpinion') }}</strong>
								</span>
							</div>
							@endif
						</div>
						<div class="form-group">
							<label id="evaluation" for="evaluation" class="col-md-4 control-label">Avaliação do Promotor</label>
							<div class="col-md-6">
								<fieldset class="rating" id="ratingP" >
									<input type="radio" id="star5P" name="ratingP" value="5" {{old('ratingP') == 5 ? 'checked' : ''}}/><label class = "full" for="star5P" title="Muito Bom - 5 estrelas"></label>
									<input type="radio" id="star4P" name="ratingP" value="4" {{old('ratingP') == 4 ? 'checked' : ''}} /><label class = "full" for="star4P" title="Bom - 4 estrelas"></label>
									<input type="radio" id="star3P" name="ratingP" value="3" {{old('ratingP') == 3 ? 'checked' : ''}}/><label class = "full" for="star3P" title="Razoável - 3 estrela"></label>
									<input type="radio" id="star2P" name="ratingP" value="2" {{old('ratingP') == 2 ? 'checked' : ''}}/><label class = "full" for="star2P" title="Mau - 2 estrela"></label>
									<input type="radio" id="star1P" name="ratingP" value="1" {{old('ratingP') == 1 ? 'checked' : ''}}/><label class = "full" for="star1P" title="Muito Mau - 1 estrela"></label>
								</fieldset>
							</div>
							@if ($errors->has('ratingP'))
							<div class="col-xs-10 col-xs-offset-1">
								<span class="error-span">
									<strong>{{ $errors->first('ratingE') }}</strong>
								</span>
							</div>
							@endif
						</div>
						<div class="form-group">
							<label for="opinion" class="col-md-4 control-label">Opinião sobre o Promotor</label>

							<div class="col-md-7">
								<textarea id="opinion" class="form-control textareaSize" name="promoOpinion" placeholder="Deixe aqui a sua opinião sobre o Promotor do Evento!" maxlength="1000">{{old('promoOpinion')}}</textarea>

							</div>
							@if ($errors->has('promoOpinion'))
							<div class="col-xs-10 col-xs-offset-1">
								<span class="error-span">
									<strong>{{ $errors->first('promoOpinion') }}</strong>
								</span>
							</div>
							@endif
						</div>
						<hr>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4 inline">
								<button type="submit" class="button is-large is-info">Avaliar</button>
								<a href="{{ url('/event', $event->id) }}" class="button is-large is-danger">Cancelar</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection