@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/evaluations.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<div class="row">
	<div class="col-xs-10 col-xs-offset-1 panel panel-body">
		<div class="col-xs-12 alignCenter noPadding">
			<h3>{{ $event->name }}</h3>
		</div>
		<div class="col-xs-12 noPadding">
			<div class="col-md-6 col-xs-12 alignCenter noPadding">
				<h4>Avaliação Média do Evento</h4>
				<div class="col-xs-12 noPadding">
					<fieldset class="rating inline" id="ratingE" disabled>
						<input type="radio" id="star5E" name="ratingE" value="5" {{ $eventEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5E" title="Muito Bom - 5 estrelas"></label>
						<input type="radio" id="star4E" name="ratingE" value="4" {{ $eventEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4E" title="Bom - 4 estrelas"></label>
						<input type="radio" id="star3E" name="ratingE" value="3" {{ $eventEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3E" title="Razoável - 3 estrela"></label>
						<input type="radio" id="star2E" name="ratingE" value="2" {{ $eventEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2E" title="Mau - 2 estrela"></label>
						<input type="radio" id="star1E" name="ratingE" value="1" {{ $eventEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1E" title="Muito Mau - 1 estrela"></label>
					</fieldset>
				</div>
			</div>
			<div class="col-md-6 col-xs-12 alignCenter noPadding">
				<h4>Avaliação Média do Promotor</h4>
				<div class="col-xs-12 noPadding">
					<fieldset class="rating inline" id="ratingP" disabled>
						<input type="radio" id="star5P" name="ratingP" value="5" {{ $promotorEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5P" title="Muito Bom - 5 estrelas"></label>
						<input type="radio" id="star4P" name="ratingP" value="4" {{ $promotorEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4P" title="Bom - 4 estrelas"></label>
						<input type="radio" id="star3P" name="ratingP" value="3" {{ $promotorEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3P" title="Razoável - 3 estrela"></label>
						<input type="radio" id="star2P" name="ratingP" value="2" {{ $promotorEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2P" title="Mau - 2 estrela"></label>
						<input type="radio" id="star1P" name="ratingP" value="1" {{ $promotorEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1" title="Muito Mau - 1 estrela"></label>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="col-xs-12 noPadding">
			<hr>
			<div class="col-xs-12">
				<div class="col-xs-6">
					<h3>Avaliações</h3>
				</div>
				<div class="col-xs-6 paddingTop alignRight">
					<a href="{{ url('/event', $event->id ) }}" class="button is-medium is-primary">Voltar ao Evento</a>
				</div>
			</div>
			
			@foreach ($evaluations as $key => $evaluation)
			<div class="col-xs-12 noPadding">
				<hr>
				<h4>Avaliação #{{ $key+1+((request('page') ? request('page')-1: 0)*10) }}</h4>
				<div class="col-xs-12 noPadding">
					<div class="col-md-6 col-xs-12 alignCenter noPadding" >
						<label class="paddingTop">Avaliação Evento:</label><br>
						<fieldset class="rating inline" id="rating{{$evaluation->id}}E" disabled>
							<input type="radio" id="star5{{$evaluation->id}}E" name="rating{{$evaluation->id}}E" value="5" {{ $evaluation->eventEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5{{$evaluation->id}}E" title="Muito Bom - 5 estrelas"></label>
							<input type="radio" id="star4{{$evaluation->id}}E" name="rating{{$evaluation->id}}E" value="4" {{ $evaluation->eventEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4{{$evaluation->id}}E" title="Bom - 4 estrelas"></label>
							<input type="radio" id="star3{{$evaluation->id}}E" name="rating{{$evaluation->id}}E" value="3" {{ $evaluation->eventEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3{{$evaluation->id}}E" title="Razoável - 3 estrela"></label>
							<input type="radio" id="star2{{$evaluation->id}}E" name="rating{{$evaluation->id}}E" value="2" {{ $evaluation->eventEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2{{$evaluation->id}}E" title="Mau - 2 estrela"></label>
							<input type="radio" id="star1{{$evaluation->id}}E" name="rating{{$evaluation->id}}E" value="1" {{ $evaluation->eventEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1{{$evaluation->id}}E" title="Muito Mau - 1 estrela"></label>
						</fieldset>
					</div>
					<div class="col-md-6 col-xs-12 alignCenter noPadding">
						<label class="paddingTop">Avaliação Promotor:</label><br>
						<fieldset class="rating inline" id="rating{{$evaluation->id}}P" disabled>
							<input type="radio" id="star5{{$evaluation->id}}P" name="rating{{$evaluation->id}}P" value="5" {{ $evaluation->promotorEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5{{$evaluation->id}}P" title="Muito Bom - 5 estrelas"></label>
							<input type="radio" id="star4{{$evaluation->id}}P" name="rating{{$evaluation->id}}P" value="4" {{ $evaluation->promotorEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4{{$evaluation->id}}P" title="Bom - 4 estrelas"></label>
							<input type="radio" id="star3{{$evaluation->id}}P" name="rating{{$evaluation->id}}P" value="3" {{ $evaluation->promotorEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3{{$evaluation->id}}P" title="Razoável - 3 estrela"></label>
							<input type="radio" id="star2{{$evaluation->id}}P" name="rating{{$evaluation->id}}P" value="2" {{ $evaluation->promotorEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2{{$evaluation->id}}P" title="Mau - 2 estrela"></label>
							<input type="radio" id="star1{{$evaluation->id}}P" name="rating{{$evaluation->id}}P" value="1" {{ $evaluation->promotorEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1{{$evaluation->id}}P" title="Muito Mau - 1 estrela"></label>
						</fieldset>
					</div>
				</div>
				<div class="col-xs-12 noPadding">
					<div class="col-md-6 col-xs-12 alignCenter">
						<br>
						<label>Opinião Evento</label>
						<br>
						@if($evaluation->eventOpinion)
						<p class="wordwrap">{{ $evaluation->eventOpinion }}</p>
						@else
						<p class="wordwrap">Não foi dada uma opinião!</p>
						@endif
					</div>

					<div class="col-md-6 col-xs-12 alignCenter">
						<br>
						<label>Opinião Promotor</label>
						<br>
						@if($evaluation->promotorOpinion)
						<p class="wordwrap">{{ $evaluation->promotorOpinion }}</p>
						@else
						<p class="wordwrap">Não foi dada uma opinião!</p>
						@endif
					</div>
				</div>
				
			</div>
			@endforeach
			<div class="col-xs-12 alignCenter noPadding">
				{{ $evaluations->links() }}
			</div>
		</div>
		
	</div>
</div>
@endsection