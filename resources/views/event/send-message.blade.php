@extends('promotor.sidebar')

@section('contentPromotor')
<link href="{{ URL::asset('/css/send-message.css') }}" rel="stylesheet">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel panel-body">
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/event/'.$event->id.'/sendmessage') }}">
				{{ csrf_field() }}

				<div class="form-group alignCenter">
					<div class="col-md-12">
						<h3>{{ $event->name }}</h3>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Data de Inicio</label>

					<div class="col-md-6">
						<h4>{{ $event->initDate }}</h4>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Data de Fim</label>

					<div class="col-md-6">
						<h4>{{ $event->finishDate }}</h4>
					</div>
				</div>

				<hr>
				<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
					<label for="message" class="col-md-4 control-label">Mensagem</label>

					<div class="col-md-6">
						<textarea id="message" class="form-control" name="message" maxlength="1000" placeholder="Insira aqui a mensagem a enviar!" value="{{ old('message') }}" required></textarea>

						@if ($errors->has('message'))
						<span class="help-block">
							<strong>{{ $errors->first('message') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4 inline">
						<button type="submit" class="button is-large is-info">
							Enviar
						</button>
						<a href="{{ url('/event', $event->id) }}" class="button is-large is-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection