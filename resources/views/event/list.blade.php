@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-datetimepicker-build.css') }}">
<div id="vue">
	<list-events categories="{{json_encode($categories)}}" districts="{{json_encode($districts)}}" tags="{{json_encode($tags)}}"  base_url="/events"></list-events>
</div>
<script src="{{ URL::asset('/js/public_event_list.js') }}"></script>
@endsection