@extends('promotor.sidebar')

@section('contentPromotor')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-datetimepicker-build.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script src="https://use.fontawesome.com/1cf3fe9508.js"></script>
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>

<div class="row">
	<div class="col-md-12">

				<div id="vue">
					<edit-event categories="{{json_encode($categories)}}"
									districts="{{json_encode($districts)}}" encoded='{{json_encode($event)}}'>
					</edit-event>
				</div>
			</div>
	</div>
</div>
<script src="{{ URL::asset('/js/edit_event.js') }}"></script>
@endsection