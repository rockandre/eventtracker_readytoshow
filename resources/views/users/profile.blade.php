@extends('users.sidebar')

@section('contentUser')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-datetimepicker-build.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script src="https://use.fontawesome.com/1cf3fe9508.js"></script>
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>
@if($messageS = Session::get('messageS'))
<div align="center" class="alert alert-success">
	<b>{{$messageS}}</b>
</div>
@endif
<div id="vue">
	<user-profile auth_user="{{json_encode($user)}}" districts="{{json_encode($districts)}}"></user-profile>
</div>

<script src="{{ URL::asset('/js/user_profile.js') }}"></script>
@endsection