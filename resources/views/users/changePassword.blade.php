@extends('users.sidebar')

@section('contentUser')
<link href="{{ URL::asset('/css/settings.css') }}" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">

<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>


<div id="vue">

	<change-password></change-password>

</div>

<script src="{{ URL::asset('/js/change_password.js') }}"></script>

@endsection