@extends('administrator.admin-panel')

@section('contentAdmin')
<link href="{{ URL::asset('/css/administrator.css') }}" rel="stylesheet">
<div class="col-md-12 col-xs-12">
    <div class="col-md-12">
        <h4>Lista de Eventos Ocultados</h4>
    </div>
    <div>
    <form class="form-vertical" role="form" method="get" action="{{ route('admin.events.hidden', ['searchText' => request('searchText')])}}">
            @include('administrator.events.partials.filter')
        </form>
    </div>
    @if(count($events) == 0)
    <div class="alert alert-info" align="center">
        <strong>Alerta!</strong> Não foram encontrados Eventos!
    </div>

    @else
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Promotor</th>
                    <th>Nome</th>
                    <th>Data de Inicio</th>
                    <th>Data de Fim</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($events as $event)
                <tr>
                    <td>{{ $event->id }}</td>
                    <td>{{ $event->user->name }}</td>
                    <td>{{ $event->name }}</td>
                    <td>{{ $event->initDate }}</td>
                    <td>{{ $event->finishDate }}</td>
                    <td class="buttonsArea">
                        <div class="inline">
                            <a href="{{ url('/event/'. $event->id) }}" class="button is-medium is-info">Visualizar</a>
                            <form class="inline" action="{{ url('admin/event/toogleVisibility/'.$event->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" name="{{$event->id}}" class="button is-medium is-success">Visivel</button>
                            </form>
                            <button class="button is-medium is-danger">Eliminar</button>
                            <a href="{{ url('/user/profile/'. $event->user->id) }}" class="button is-medium is-primary">Perfil Promotor</a>
                        </div>
                    </td>

                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    <div align="center">
        {{ $events->links() }}
    </div>
    @endif
</div>


@endsection