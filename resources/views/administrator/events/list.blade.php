@extends('administrator.admin-panel')

@section('contentAdmin')

<div class="col-md-12 col-xs-12">

    <div class="col-md-12">
        <h3>Lista de Eventos</h3>
    </div>
    <div>
        <form class="form-vertical" role="form" method="get" action="{{ route('admin.events.list', ['searchText' => request('searchText')])}}">
            @include('administrator.events.partials.filter')
        </form>
    </div>
    @if(count($events) == 0)
    <div class="alert alert-info" align="center">
        <strong>Alerta!</strong> Não foram encontrados Eventos!
    </div>

    @else
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Promotor</th>
                    <th>Nome</th>
                    <th>Data de Inicio</th>
                    <th>Data de Fim</th>
                    <th>Estado</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($events as $event)
                <tr>
                    <td>{{ $event->id }}</td>
                    <td>{{ $event->user->name }}</td>
                    <td>{{ $event->name }}</td>
                    <td>{{ $event->initDate }}</td>
                    <td>{{ $event->finishDate }}</td>
                    <td>{{ $event->statusToStr() }}</td>
                    <td class="buttonsArea">
                        <div class="inline">
                            <a href="{{ url('/event/'. $event->id) }}" class="button is-medium is-info">Detalhes</a>
                            @if (!$event->hidden)
                            <button name="{{$event->id}}" class="hideEvent button is-medium is-warning">Ocultar</button>

                            <form id="hideEvent-form-{{$event->id}}" action="{{ url('admin/event/toogleVisibility/'.$event->id) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            @else
                            
                            <form class="inline" action="{{ url('admin/event/toogleVisibility/'.$event->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" name="{{$event->id}}" class="button is-medium is-success">Visivel</button>
                            </form>
                            @endif
                            <button name="{{$event->id}}" class="deleteEvent button is-medium is-danger">Eliminar</button>

                            <form id="deleteEvent-form-{{$event->id}}" action="{{ url('admin/event/delete/'.$event->id) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            <a href="{{ url('/user/profile/'. $event->user->id) }}" class="button is-medium is-primary">Perfil Promotor</a>
                        </div>
                    </td>

                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    <div align="center">
        {{ $events->links() }}
    </div>
    @endif
</div>

@endsection