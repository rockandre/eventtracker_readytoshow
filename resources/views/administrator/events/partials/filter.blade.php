
<table class="table">
  <thead>
    <tr class="filters">
      <th>
      <input id="searchText" type="text" class="form-control" name="searchText" value="{{ request('searchText') }}" placeholder="Insira o ID ou o nome do evento!">
      </th>
      <th>
        <button type="submit" class="button is-large is-info">
          Pesquisar
        </button>
      </th>
    </tr>
  </thead>
</table>