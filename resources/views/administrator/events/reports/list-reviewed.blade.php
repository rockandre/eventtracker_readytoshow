@extends('administrator.admin-panel')

@section('contentAdmin')
<div class="col-md-12 col-xs-12">
	<div class="col-md-12">
		<h4>Lista de Denúncias Revistas</h4>
	</div>
	<div>
		<form class="form-vertical" role="form" method="get" action="{{ route('admin.events.reports.notReviewed', ['searchText' => request('searchText')])}}">
			@include('administrator.events.partials.filter')
		</form>
	</div>
	@if(count($reports) == 0)
	<div class="alert alert-info" align="center">
		<strong>Alerta!</strong> Não foram encontrados Denúncias Revistas!
	</div>

	@else
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>ID Denúncia</th>
					<th>ID Evento</th>
					<th>Nome Evento</th>
					<th>Data de Denúncia</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($reports as $report)
				<tr>
					<td>{{ $report->id }}</td>
					<td>{{ $report->event->id }}</td>
					<td>{{ $report->event->name }}</td>
					<td>{{ $report->created_at }}</td>
					<td class="buttonsArea">
						<div class="inline">
							<a href="{{ url('/admin/events/report/'. $report->id) }}" class="button is-medium is-primary">Ver Detalhes</a>
						</div>
					</td>

				</tr>

				@endforeach
			</tbody>
		</table>
	</div>
	<div align="center">
		{{ $reports->links() }}
	</div>
	@endif
</div>

@endsection