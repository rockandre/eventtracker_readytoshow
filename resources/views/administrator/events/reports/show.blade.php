@extends('administrator.admin-panel')

@section('contentAdmin')
<link href="{{ URL::asset('/css/report-event.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">Denúncia #{{ $report->id }}</div>
			<div class="panel-body">
				<div class="form-horizontal col-md-12">
					<h3>Evento</h3>
					<div class="form-group paddingTop">
						<label class="col-md-4 control-label">Nome do Evento</label>

						<div class="col-md-6">
							<label class="control-label">{{ $report->event->name }}</label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Data de Inicio</label>

						<div class="col-md-6">
							<label class="control-label">{{ $report->event->initDate }}</label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Data de Fim</label>

						<div class="col-md-6">
							<label class="control-label">{{ $report->event->finishDate }}</label>
						</div>
					</div>

					<hr>
					<h3>Denúncia</h3>

					<div class="form-group">
						<label for="reason" class="col-md-4 control-label">Data da Denúncia</label>

						<div class="col-md-6">
							<label class="control-label">{{ $report->created_at }}</label>
						</div>
					</div>

					<div class="form-group">
						<label for="reason" class="col-md-4 control-label">Razão da Denúncia</label>

						<div class="col-md-6">
							<label class="control-label">{{ $report->reason }}</label>
						</div>
					</div>

					<hr>
					<div class="buttonsArea form-group">
						<div class="col-md-6 col-md-offset-2 inline">
							<a href="{{ url('/event', $report->event->id) }}" class="button is-medium is-primary">Detalhes Evento</a>
							@if(!$report->reviewed)
							<form class="inline" action="{{ url('admin/event/report/reviewed/'.$report->id) }}" method="POST">
								{{ csrf_field() }}
								<button type="submit" name="{{$report->id}}" class="button is-medium is-success">Marcar como Revista</button>
							</form>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection