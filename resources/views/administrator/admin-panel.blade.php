@extends('layouts.app')

@section('admin')
<link href="{{ URL::asset('/css/administrator.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<div class="container-fluid main-container">
	<div class="col-md-2 col-xs-12 col-sm-12 sidebar">
		<div class="row">
			<!-- Menu -->
			<div class="side-menu">
				<nav class="navbar navbar-default" role="navigation">
					<!-- Main Menu -->
					<div class="side-menu-container">
						<ul class="nav navbar-nav">
							<li class="{{ $currentView=='dashboard' ? 'active' : ''}}"><a href="{{ url('/admin/dashboard') }}"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>

							<!-- Dropdown-->
							<li class="panel panel-default" id="dropdown">
								<a data-toggle="collapse" href="#dropdown-events">
									<span class="glyphicon glyphicon-globe"></span>Eventos<span class="caret"></span>
								</a>
								<div id="dropdown-events" class="panel-collapse collapse {{$currentView=='listEvents' || $currentView=='reportedNotReviewed' || $currentView=='reportedReviewed' || $currentView=='hiddenEvents' ? 'in' : '' }}" expanded="false" aria-expanded="{{$currentView=='listEvents' || $currentView=='reportedNotReviewed' || $currentView=='reportedReviewed' || $currentView=='hiddenEvents' ? 'true' : 'false' }}">
									<div class="panel-body">
										<ul class="nav navbar-nav">
											<li class="{{ $currentView=='listEvents' ? 'active' : ''}}"><a href="{{ url('admin/events/list') }}"><span class="glyphicon glyphicon-list-alt"></span>Lista de Eventos</a></li>
											<li class="panel panel-default" id="dropdown">
												<a data-toggle="collapse" href="#dropdown-reports">
													<span class="glyphicon glyphicon-warning-sign"></span>Denuncias<span class="caret"></span>
												</a>
												<div id="dropdown-reports" class="panel-collapse collapse {{$currentView=='reportedNotReviewed' || $currentView=='reportedReviewed' ? 'in' : '' }}" expanded="false" aria-expanded="{{$currentView=='reportedNotReviewed' || $currentView=='reportedReviewed' ? 'true' : 'false' }}">
													<div class="panel-body">
														<ul class="nav navbar-nav">
															<li class="{{ $currentView=='reportedNotReviewed' ? 'active' : ''}}"><a href="{{ url('/admin/events/reports/notReviewed') }}"><span class="glyphicon glyphicon-pencil"></span>Não Revistas</a></li>
															<li class="{{ $currentView=='reportedReviewed' ? 'active' : ''}}"><a href="{{ url('/admin/events/reports/reviewed') }}"><span class="glyphicon glyphicon-ok"></span>Revistas</a></li>
														</ul>
													</div>
												</div>
											</li>
											<li class="{{ $currentView=='hiddenEvents' ? 'active' : ''}}"><a href="{{ url('admin/events/hidden') }}"><span class="glyphicon glyphicon-eye-close"></span>Ocultados</a></li>
										</ul>
									</div>
								</div>
							</li>

							<li class="panel panel-default" id="dropdown">
								<a data-toggle="collapse" href="#dropdown-users">
									<span class="glyphicon glyphicon-user"></span>Utilizadores<span class="caret"></span>
								</a>
								<div id="dropdown-users" class="panel-collapse collapse {{ $currentView=='listUsers' || $currentView=='blockedUsers' ? 'in' : '' }}" expanded='false' area-expanded="{{ $currentView=='listUsers' || $currentView=='blockedUsers' ? 'true' : 'false'}}">
									<div class="panel-body">
										<ul class="nav navbar-nav">
											<li class="{{ $currentView=='listUsers' ? 'active' : ''}}"><a href="{{ url('admin/users/list') }}"><span class="glyphicon glyphicon-list-alt"></span>Lista de Utilizadores</a></li>
											<li class="{{ $currentView=='blockedUsers' ? 'active' : ''}}"><a href="{{ url('admin/users/blocked') }}"><span class="glyphicon glyphicon-ban-circle"></span>Bloqueados</a></li>
										</ul>
									</div>
								</div>
							</li>

							<li class="{{ $currentView=='newAdmin' ? 'active' : ''}}"><a href="{{ url('admin/new') }}"><span class="glyphicon glyphicon-plus-sign"></span>Novo Administrador</a></li>

						</ul>
					</div>
				</nav>

			</div>
		</div>  		
	</div>
	<div class="col-md-10 col-xs-12 content">
		@yield('contentAdmin')
	</div>
</div>

<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>
<script src="{{ URL::asset('/js/administrator.js') }}"></script>
@endsection