@extends('administrator.admin-panel')

@section('contentAdmin')
@if($newusers>0 || $newevents>0 || $newreports>0 )
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-body" >

		<div>
			<a class="dashboardButtons btn btn-default col-md-2 col-md-offset-0 col-xs-10 col-xs-1" href="{{ url('/admin/dashboard') }}"><span class="glyphicon glyphicon-dashboard"></span> Vista Inicial</a>
		</div>

		<div>
			<a class=" dashboardButtons btn btn-default col-md-3 col-md-offset-0 col-xs-10 col-xs-1" href="{{ url('/admin/dashboard/newusers') }}"><span class="glyphicon glyphicon-user"></span> Novos Utilizadores <span class="label label-primary">{{ $newusers }}</span></a>
		</div>



		<div>
			<a class=" dashboardButtons btn btn-default col-md-3 col-md-offset-0 col-xs-10 col-xs-1 " href="{{ url('/admin/dashboard/newevents') }}"><span class="glyphicon glyphicon-globe"></span> Novos Eventos <span class="label label-primary">{{ $newevents }}</span></a>
		</div>


		<div>
			<a class="dashboardButtons btn btn-default col-md-3 col-md-offset-0 col-xs-10 col-xs-1" href="{{ url('/admin/dashboard/newreports') }}"><span class="glyphicon glyphicon-user"></span> Novas Denuncias <span class="label label-primary">{{ $newreports }}</span></a>
		</div>


	</div>
</div>
@endif
<div class="col-md-12">
	<div class="content">
		@yield('contentDashboard')
	</div>
</div>
@endsection