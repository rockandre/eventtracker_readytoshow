@extends('administrator.admin-panel')

@section('contentAdmin')
<div class="col-md-12 col-xs-12">
	<div class="col-md-12">
        <h4>Lista de Utilizadores Bloqueados</h4>
    </div>
	<div>
		<form class="form-vertical" role="form" method="get" action="{{ route('admin.users.list', ['searchText' => request('searchText')])}}">
			@include('administrator.users.partials.filter')
		</form>
	</div>
	@if(count($users) == 0)
	<div class="alert alert-info" align="center">
	<strong>Alerta!</strong> Não foram encontrados Utilizadores!
	</div>

	@else
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Data de Registo</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at }}</td>
					<td class="buttonsArea">
						<a href="{{ url('/user/profile/'. $user->id) }}" class="button is-medium is-primary">Ver Perfil</a>
						<form class="inline" action="{{ url('admin/user/toogleBlock/'.$user->id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" name="{{$user->id}}" class="button is-medium is-success">Desbloquear</button>
						</form>
					</td>

				</tr>

				@endforeach
			</tbody>
		</table>
	</div>
	<div align="center">
		{{ $users->links() }}
	</div>
	@endif
</div>

@endsection