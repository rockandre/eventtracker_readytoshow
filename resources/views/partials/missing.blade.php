@extends('layouts.app')

@section('content')
	<div class="row" align="center">
		<h3>404
OOOPS, NÃO DISPONÍVEL...<br><br>
A página solicitada não foi encontrada. Nós temos alguns palpites.<br>
Se você digitou o URL diretamente, poderá não ter sido escrito corretamente.<br>
Se você clicou num link para chegar até aqui, o link encontra-se indisponível.<br>
O que você pode fazer?<br>
Continue a navegar e encontre as melhores oportunidades.<br><br>

Volte atrás para a página anterior.<br>
Utilize a barra de navagação para ir para páginas disponiveis!</h3>
	</div>
@endsection