@component('mail::message')
A sua compra foi efetuada com sucesso

Em anexo seguem o(s) seu(s) bilhete(s),

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
