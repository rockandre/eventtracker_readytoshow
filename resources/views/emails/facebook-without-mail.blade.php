@component('mail::message')
Bem vindo á plataforma Event Tracker

Para fazer o registo na nossa plataforma necessita de seguir os passos seguintes:

Pressione o botão para ser redirecionado para a nossa plataforma e finalizar o registo.
@component('mail::button', ['url' => $url])
Registar
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
