@component('mail::message')
Caro/a {{ $user->name }},

O promotor do evento {{ $event->name }} deseja transmitir-lhe a seguinte mensagem:

@component('mail::panel')
{{ $message }}
@endcomponent

@component('mail::button', ['url' => 'eventtracker.app/event/'.$event->id])
Visualizar Evento
@endcomponent

Com as melhores saudações,<br>
Equipa {{ config('app.name') }}
@endcomponent
