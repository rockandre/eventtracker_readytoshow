@extends('promotor.sidebar')

@section('contentPromotor')
<link href="{{ URL::asset('/css/promotor-stats.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<div class="row">
	<div class="col-xs-12 panel panel-body">
		<div class="col-xs-12" align="center">
			<h3>Estatísticas</h3>
		</div>
		<div class="col-xs-12">
			<div class="col-md-2 col-xs-12" align="center">
				<h4>Eventos</h4>
				<h4>{{ $promotorEventsCount }}</h4> 
			</div>
			<div class="col-md-5 col-xs-12" align="center">
				<h4>Avaliação Média dos Eventos</h4>
				@if($eventEval == 0)
				<p><b>Ainda sem Avaliações!</b></p>
				@else
				<fieldset class="rating inline" id="ratingE" disabled>
					<input type="radio" id="star5E" name="ratingE" value="5" {{ $eventEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5E" title="Muito Bom - 5 estrelas"></label>
					<input type="radio" id="star4E" name="ratingE" value="4" {{ $eventEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4E" title="Bom - 4 estrelas"></label>
					<input type="radio" id="star3E" name="ratingE" value="3" {{ $eventEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3E" title="Razoável - 3 estrela"></label>
					<input type="radio" id="star2E" name="ratingE" value="2" {{ $eventEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2E" title="Mau - 2 estrela"></label>
					<input type="radio" id="star1E" name="ratingE" value="1" {{ $eventEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1E" title="Muito Mau - 1 estrela"></label>
				</fieldset>
				@endif
			</div>
			<div class="col-md-5 col-xs-12" align="center">
				<h4>Avaliação Média do Promotor</h4>
				@if($promotorEval == 0)
				<p><b>Ainda sem Avaliações!</b></p>
				@else
				<fieldset class="rating inline" id="ratingP" disabled>
					<input type="radio" id="star5P" name="ratingP" value="5" {{ $promotorEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5P" title="Muito Bom - 5 estrelas"></label>
					<input type="radio" id="star4P" name="ratingP" value="4" {{ $promotorEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4P" title="Bom - 4 estrelas"></label>
					<input type="radio" id="star3P" name="ratingP" value="3" {{ $promotorEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3P" title="Razoável - 3 estrela"></label>
					<input type="radio" id="star2P" name="ratingP" value="2" {{ $promotorEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2P" title="Mau - 2 estrela"></label>
					<input type="radio" id="star1P" name="ratingP" value="1" {{ $promotorEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1P" title="Muito Mau - 1 estrela"></label>
				</fieldset>
				@endif
			</div>
		</div>
		<div class="col-xs-12">
			<hr>
			<div id="vue">
				<stats-promotor promotor_stats_avg="{{ json_encode($promotorStatsAvg) }}" promotor_stats_total="{{ json_encode($promotorStatsTotal) }}"></stats-promotor>
			</div>
		</div>
	</div>
	<div class="col-xs-12 panel panel-body">
		<h4><b>Avaliações</b></h4>
		@if(count($evaluations)==0)
		<p>Ainda não foram efetuadas Avaliações!</p>
		@else
		@foreach ($evaluations as $evaluation)
		<div class="col-xs-12">
			<hr>
			<div class="col-md-6 col-xs-12 alignCenter">
				<label>Evento:</label><h4>{{ $evaluation->event->name }}</h4>
			</div>
			<div class="col-md-6 col-xs-12 alignCenter">
				<label>Avaliação Promotor:</label><br> 
				<fieldset class="rating inline" id="rating{{$evaluation->id}}" disabled>
					<input type="radio" id="star5{{$evaluation->id}}" name="rating{{$evaluation->id}}" value="5" {{ $evaluation->promotorEval == 5 ? 'checked' : ''}}/><label class = "full" for="star5{{$evaluation->id}}" title="Muito Bom - 5 estrelas"></label>
					<input type="radio" id="star4{{$evaluation->id}}" name="rating{{$evaluation->id}}" value="4" {{ $evaluation->promotorEval == 4 ? 'checked' : ''}}/><label class = "full" for="star4{{$evaluation->id}}" title="Bom - 4 estrelas"></label>
					<input type="radio" id="star3{{$evaluation->id}}" name="rating{{$evaluation->id}}" value="3" {{ $evaluation->promotorEval == 3 ? 'checked' : ''}}/><label class = "full" for="star3{{$evaluation->id}}" title="Razoável - 3 estrela"></label>
					<input type="radio" id="star2{{$evaluation->id}}" name="rating{{$evaluation->id}}" value="2" {{ $evaluation->promotorEval == 2 ? 'checked' : ''}}/><label class = "full" for="star2{{$evaluation->id}}" title="Mau - 2 estrela"></label>
					<input type="radio" id="star1{{$evaluation->id}}" name="rating{{$evaluation->id}}" value="1" {{ $evaluation->promotorEval == 1 ? 'checked' : ''}}/><label class = "full" for="star1{{$evaluation->id}}" title="Muito Mau - 1 estrela"></label>
				</fieldset>
			</div>
			<div class="col-md-12 col-xs-12">
				<br>
				<p class="wordwrap">{{ $evaluation->promotorOpinion }}</p>
			</div>
		</div>
		@endforeach

		<div align="center">
			{{ $evaluations->links() }}
		</div>
		@endif
	</div>
</div>

<script src="{{ URL::asset('/js/promotor_stats.js') }}"></script>
@endsection