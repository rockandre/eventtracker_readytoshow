@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/promotor-area.css') }}" rel="stylesheet">
<div>
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="row profile">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="{{ URL::asset('/images/profile/'. Auth::user()->image_id) }}" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">

					</div>
					<div class="profile-usertitle-job">
						Promotor de Eventos
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a href="{{url('/user/personal/profile')}}" type="button" class="button is-success is-medium ">
						Ver Perfil
					</a>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="{{ $currentView == 'create' ? 'active' : '' }}">
							<a href="{{url('/promotorView/create')}}">
								Criar um evento
							</a>
						</li>
						<li class="{{ $currentView == 'advertised' ? 'active' : '' }}">
							<a href="{{url('/promotorView/advertised/')}}">
								Eventos Publicitados 
							</a>
						</li>
						<li class="{{ $currentView == 'canceled' ? 'active' : '' }}">
							<a href="{{url('/promotorView/canceled')}}">
								Eventos Cancelados
							</a>
						</li>
						<li class="{{ $currentView == 'finished' ? 'active' : '' }}">
							<a href="{{url('/promotorView/finished')}}">
								Eventos Terminados
							</a>
						</li>
						<li class="{{ $currentView == 'ticketmanagement' ? 'active' : '' }}">
							<a href="{{url('/promotorView/ticketmanagement')}}">
								Gestão de Bilheteira
							</a>
						</li>
						<li class="{{ $currentView == 'stats' ? 'active' : '' }}">
							<a href="{{url('/promotorView/stats')}}">
								Estatisticas
							</a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 col-xs-12">
		@yield('contentPromotor')
	</div>
</div>
@endsection