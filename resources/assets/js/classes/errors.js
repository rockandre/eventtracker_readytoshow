'use strict';

/** Helper class for Errors **/

class Errors {

    constructor(){

        this.errors = {};

    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

    has(field){
        
        return this.errors.hasOwnProperty(field);
    }

    get(field) {

        if(this.errors[field]) {
            return this.errors[field][0];
        }
    }

    hasMultimedia(index){
        return this.errors.hasOwnProperty('multimedia.'+index+'.name') ||
        this.errors.hasOwnProperty('multimedia.'+index+'.url');
    }

    getMultimedia(index)
    {
        return this.errors['multimedia.'+index+'.name'] === undefined ? 
        this.errors['multimedia.'+index+'.url'][0]
        : this.errors['multimedia.'+index+'.name'][0];
    }

    clearMultimedia(index)
    {
        delete this.errors['multimedia.'+index+'.url'];
        delete this.errors['multimedia.'+index+'.name'];
    }

    clear(field) {
        delete this.errors[field];
    }

    record(errors) {

        this.errors = errors;
    }

    set(field, error) {
        this.errors[field] = []
        this.errors[field].push(error);
    }
};

module.exports = Errors;