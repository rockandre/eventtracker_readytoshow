'use strict';

/** Helper class for Errors **/

class User  {

    constructor(){

        this.name =  '';

        this.email = '';

        this.gender = '';

        this.district = 1;

        this.address =  '';

        this.nif =  '';

        this.birthdate =  '';

        this.password = '';

        this.password_confirmation = '';

        this.profileImage = '/images/profile/default.jpg';

        this.description = '';
        
    }

    set(field, value){

        this[field] = value;
    }

    get(field){

        if(this[field]) {
            return this[field];
        }
    }

    parse(auth_user)
    {
        auth_user = JSON.parse(auth_user);
        this.name = auth_user.name;
        this.email = auth_user.email;
        this.gender = auth_user.gender;
        this.district = auth_user.district_id;
        this.address = auth_user.address;
        this.nif = auth_user.NIF;

        if(auth_user.birthdate != null)
        {
            this.birthdate = new Date(auth_user.birthdate);
        }
        
        this.profileImage = '/images/profile/'+auth_user.image_id;
        this.description = auth_user.description;
    }
    
};


module.exports = User;