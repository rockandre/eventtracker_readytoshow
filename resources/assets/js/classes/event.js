'use strict';

/** Helper class for Errors **/

class Event  {

    constructor(){

        this.id = -1;

        this.address =  '';

        this.category = 1;

        this.description = '';

        this.district = 1;

        this.evalTime = 7;

        this.evalType =  undefined;

        this.finishDate =  undefined;

        this.hasTickets =  undefined; 

        this.initDate = undefined;

        this.image = null;

        this.hasImage = false;

        this.location = undefined;

        this.multimedia = [{name: "", url:""}];

        this.name = undefined;

        this.spaceName =  '';

        this.hasSuggestedCategory = false;

        this.newCategory = undefined;

        this.tags = [];

        this.ticketManagement = '';

        this.ticketSaleDescription = '';

        this.ticketsForSale = undefined;

        this.ticketSales = ['',];

        this.tickets = [];

        this.standardTicket = undefined;

    }

    set(field, value){

        this[field] = value;
    }

    get(field){

        if(this[field]) {
            return this[field];
        }
    }

    addTicketSale(ticketSale){


        if(this.ticketSales[this.ticketSales.length - 1].trim()){
            this.ticketSales.push(ticketSale);
            return true;
        }
        else{
            return 'Por favor preencha o campo anteriormente disponbilizado'
        }
    }

    removeTicketSale(index){
        if(this.ticketSales.length > 1){
            this.ticketSales.splice(index, 1);
            return true;
        }
        return false;
    }

    parseForTicketSales(event){
        event = JSON.parse(event);
        this.commonParse(event);

        for (var i = 0; i < event.tickets.length; i++) {
            this.tickets.push(event.tickets[i]);
        }

    }

    commonParse(event){
       this.id = event.id;
       this.address =  event.address;
       this.category = event.category_id;
       this.district = event.district_id;
       this.initDate = event.initDate;
       this.finishDate = event.finishDate;
       this.imagePath = event.image_id;
       this.name = event.name;
       this.spaceName = event.spaceName;
       this.tags = event.tags;

       this['categoryName'] = event.categoryName;
   }

   parse(event)
   {
    event = JSON.parse(event);
    this.commonParse(event);

    this.description = event.description;
    this.evalType = event.evalType;
    this.evalTime = event.evalTime;
    this.hasTickets = event.hasTickets;
    
    this.location = {lat: event.lat, lng: event.lng};


    if(Object.keys(event.multimedia).length > 0)
    {   
        this.multimedia = [];
        for(var key in event.multimedia)
        {
            this.multimedia.push({name:key, url:event.multimedia[key]});
        }
    }

    if(event.hasTickets)
    {
        this.ticketManagement = event.ticketManagement;           
        console.log(this);
        if(this.ticketManagement == 'external')
        {
            this.ticketsForSale = event.ticketsForSale;
            if(event.ticketSales !== undefined)
            {
                this.ticketSales = event.ticketSales;
                this.ticketSaleDescription = event.ticketSaleDescription;
            }
        }
        else{

            for (var i = event.tickets.length - 1; i >= 0; i--) {
                if(!event.tickets[i].is_standard)
                {
                    this.tickets.push(event.tickets[i]);
                }
                else{
                    this.standardTicket = event.tickets[i];
                }
            }

        }
    }
}

};


module.exports = Event;