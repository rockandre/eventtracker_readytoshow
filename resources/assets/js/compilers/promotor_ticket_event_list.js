 require('./vue_bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('event-list-wrapper', require('../components/promotor/tickets/event-list-wrapper.vue'));

Vue.component('event-list', require('../components/events/event-list.vue'));

Vue.component('ticket-event', require('../components/promotor/tickets/ticket-event.vue'));

Vue.component('event', require('../components/events/event.vue'));

Vue.component('promotor-event', require('../components/promotor/promotor-event.vue'));

Vue.component('order', require('../components/users/orders/order.vue'));

Vue.component('following-event', require('../components/users/following-event.vue'));

 const app = new Vue({
 	el: '#vue'

 });
