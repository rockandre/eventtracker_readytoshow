require('./vue_bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('event-options', require('../components/events/event-options.vue'));

 Vue.component('google-maps', require('../components/google-maps.vue'));

 Vue.component('google-maps-wrapper', require('../components/google-maps-wrapper.vue'));

 const maps = new Vue({
 	el: '#gmaps-vue'

 });

const app = new Vue({
	el: '#vue'
});

