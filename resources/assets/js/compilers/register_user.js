require('./vue_bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('register-user', require('../components/auth/register.vue'));

 Vue.component('file-upload', require('../components/File-Upload.vue'));

 const app = new Vue({
 	el: '#vue'

 });
