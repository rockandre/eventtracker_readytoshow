<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');

Route::get('/users/facebook/accountCreation/{token}', 'Auth\RegisterController@createFacebookAccount');

Route::get('/register/setFacebookEmail/{id}', 'SocialAuthController@setFacebookEmail');

Route::post('/register/setFacebookEmail/{id}', 'SocialAuthController@saveFacebookEmail');

Route::post('/createAccount', 'Auth\RegisterController@createAccountThroughMail');

Route::get('/saveRegistedPassword/{token}', 'Auth\RegisterController@saveRegistedPassword');
//Users

Route::get('/user/profile/{id}', 'UserController@show');

Route::get('/event/featured', 'EventController@getFeaturedEvent');

Route::group(['middleware' => ['registeredUserOnly']], function() {

	Route::get('/user/personal/profile', 'UserController@profile');

	Route::get('/user/personal/orders', 'UserController@orders');

	Route::post('/user/personal/orders', 'UserController@getOrders');

	Route::get('/user/personal/following', 'UserController@following');

	Route::post('/user/personal/profile/update', 'UserController@update');

	// [Promotion Area]
	Route::get('/promotorView/advertised', 'PromotorController@showAdvertised');

	Route::get('/promotorView/canceled', 'PromotorController@showCanceled');

	Route::get('/promotorView/finished', 'PromotorController@showFinished');

	Route::get('/promotorView/stats', 'PromotorController@showPromotorStats');

	Route::get('/promotorView/create', 'EventController@create');

	Route::get('/promotorView/ticketmanagement', 'PromotorController@showTicketEventList');

	// User settings
	Route::group(['middleware' => 'notSocialAccount'], function () {
		Route::get('/user/personal/changePassword', 'UserController@showChangePassword');
		Route::post('/user/personal/changePassword', 'UserController@storeNewPassword');
	});

	// Event Evaluations & Send Message to followers
	Route::group(['middleware' => 'ownerUserOnly'], function() {
		Route::get('/event/{id}/evaluations', 'EvaluationController@eventEvaluations');

		Route::get('/event/{id}/sendmessage', 'MessageController@showSendMailToFollowers');
		Route::post('/event/{id}/sendmessage', 'MessageController@sendMailToFollowers');

		Route::post('/event/{id}/sendchangemessage', 'EventController@sendChangeEventMessage');

		Route::post('/event/{id}/sendcanceledmessage', 'EventController@sendCanceledEventMessage');
	});


	//Edit Event
	Route::group(["middleware" => 'editEvent'], function() {

		Route::get('/event/edit/{id}', 'EventController@edit');

		Route::post('/event/update/{id}', 'EventController@update');

	});

	//Cancel Event
	Route::group(['middleware' => 'cancelEvent'], function () {

		Route::get('/event/cancel/{id}', 'CancellationsController@show');

		Route::post('/event/cancel/{id}', 'CancellationsController@store');

	});

	//Event Tickets
	Route::get('/event/{id}/tickets', 'TicketController@eventTickets');

	Route::get('/event/{id}/entrances', 'TicketController@eventEntrances');


	// [AJAX Requests]

	Route::group(['middleware' => 'ajax'], function() {

		Route::post('/promotorView/advertised', 'PromotorController@getAdvertised');

		Route::post('/promotorView/canceled', 'PromotorController@getCanceled');

		Route::post('/promotorView/finished', 'PromotorController@getFinished');

		Route::post('/promotorView/ticketmanagement', 'PromotorController@getTicketEvents');

		Route::post('/event/store', 'EventController@store');

		Route::post('/user/personal/following', 'UserController@getFollowing');

		Route::post('/event/follow/{id}', 'UserController@toggleFollow');

		Route::post('/orders/event/{id}/store', 'OrderController@store');

		Route::post('/event/{id}/searchTickets', 'TicketController@searchTickets');

		Route::post('/ticket/giveEntry/{id}', 'TicketController@giveEntry');

		Route::post('/ticket/rejectEntry/{id}', 'TicketController@rejectEntry');

		Route::post('/ticket/revertOperation/{id}', 'TicketController@revertOperation');

	});
});


//Events and event Listings
Route::group(["middleware" => 'hiddenEvent'], function() {

	Route::get('/event/{id}', 'EventController@show');
	Route::get('/ticketing/event/{id}', 'TicketController@show')->middleware('ticketManagementET');

});

Route::get('/event/report/{id}', 'ReportController@report');
Route::post('/event/report/{id}', 'ReportController@saveReport');

Route::group(['middleware' => 'notAdminAlreadyEvaluate'] , function() {

	Route::get('/event/evaluate/{id}', 'EvaluationController@evaluate');
	Route::post('/event/evaluate/{id}', 'EvaluationController@storeEvaluation');

});

//Authentication with Social Networks

Route::get('/redirect/{driver}', 'SocialAuthController@redirect');

Route::get('/callback/facebook', 'FacebookAuthController@callback');

Route::get('/callback/google', 'GoogleAuthController@callback');


// Filter page request's

Route::get('/events', 'EventController@all');

Route::post('/events', 'EventController@searchEvents');

Route::group(["middleware" => 'admin'], function() {

	Route::get('/admin/dashboard', 'AdministratorController@dashboard');

	Route::get('/admin/dashboard/newusers', 'AdministratorController@dashboardNewUsers');
	Route::get('/admin/dashboard/newevents', 'AdministratorController@dashboardNewEvents');
	Route::get('/admin/dashboard/newreports', 'AdministratorController@dashboardNewReports');

	Route::get('/admin/events/hidden', 'AdministratorController@hiddenEvents')
	->name('admin.events.hidden');

	Route::get('/admin/events/list', 'AdministratorController@listEvents')
	->name('admin.events.list');

	Route::post('/admin/event/toogleVisibility/{id}', 'AdministratorController@toogleEventVisibility');

	Route::post('/admin/event/delete/{id}', 'AdministratorController@deleteEvent');

	Route::get('/admin/events/reports/notReviewed', 'AdministratorController@reportedEventsNotReviewed')
	->name('admin.events.reports.notReviewed');

	Route::get('/admin/events/reports/reviewed', 'AdministratorController@reportedEventsReviewed')
	->name('admin.events.reports.reviewed');

	Route::get('/admin/events/report/{id}', 'ReportController@show');

	Route::post('/admin/event/report/reviewed/{id}', 'ReportController@setReviewed');

	Route::get('/admin/users/blocked', 'AdministratorController@blockedUsers');

	Route::get('/admin/users/list', 'AdministratorController@listUsers')
	->name('admin.users.list');

	Route::post('/admin/user/toogleBlock/{id}', 'AdministratorController@toogleBlock');

	Route::get('/admin/new', 'AdministratorController@newAdmin');

	Route::post('/admin/new', 'AdministratorController@createNewAdmin');

});


//Image upload

Route::get('/images/event/{filename}', function ($filename)
{
	return Image::make(Storage::disk('local')->get('public/event/' . $filename))->response();
});

Route::get('/images/profile/{filename}', function ($filename)
{
	return Image::make(Storage::disk('local')->get('public/profile/' . $filename))->response();
});

Route::get('/images/event/list/{filename}', function ($filename)
{
	return Image::make(Storage::disk('local')->get('public/event_list/' . $filename))->response();
});
