<?php

use Illuminate\Database\Seeder;

class User_EvaluationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$events = App\Evaluation::groupBy('event_id')->pluck('event_id')->toArray();
    	$users = DB::table('users')->where('is_admin', false)->pluck('id')->toArray();

    	$faker = Faker\Factory::create('pt_PT');


    	$this->command->info('Assigning anonymous events evaluations to users ...');
    	$bar = $this->command->getOutput()->createProgressBar(count($events));
    	for ($i = 0; $i < count($events); ++$i) {
    		$evaluations = App\Evaluation::where('event_id', $events[$i])->get()->toArray();
    		$usersUsed = array();
    		for($j = 0 ; $j < count($evaluations) ; $j++) {
    			if($faker->numberBetween(0,3) == 0) {
    				$user = $faker->randomElement($users);
    				if(!in_array($user, $usersUsed)) {
    					DB::table('user_evaluations')->insert($this->fakeUserEvaluation($faker, $user, $events[$i]));
    					array_push($usersUsed, $user);
    				} else {
    					$j--;
    				}
    			}
    		}
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');
    }

    private function fakeUserEvaluation(Faker\Generator $faker, $user_id, $event_id)
    {
    	$createdAt = Carbon\Carbon::now()->subDays($faker->numberBetween(0, 20));

    	return [
    		'user_id' => $user_id,
    		'event_id' => $event_id,
    		'created_at' => $createdAt,
    		'updated_at' => $createdAt
    	];
    }
}
