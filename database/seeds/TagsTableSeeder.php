<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
	private $numberOfTags = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $this->command->info('Creating '.$this->numberOfTags.' tags ...');
		$bar = $this->command->getOutput()->createProgressBar($this->numberOfTags);
		for ($i = 0; $i < $this->numberOfTags; ++$i) {
			DB::table('tags')->insert($this->fakeTag($faker));
			$bar->advance();
		}
		$bar->finish();
    	$this->command->info('');
    }

    private function fakeTag(Faker\Generator $faker)
    {
    	$tag = $faker->word;

    	return  [
    		'tag' => $tag,
    		'search_tag' => strtoupper($tag)
    	];
    }
}
