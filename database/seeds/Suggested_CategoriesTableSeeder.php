<?php

use Illuminate\Database\Seeder;

class Suggested_CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::where('category_id', null)->get()->toArray();

        $faker = Faker\Factory::create('pt_PT');

        $this->command->info('Creating and assigning suggested categories to events ...');
        $bar = $this->command->getOutput()->createProgressBar(count($events));
        for ($i = 0; $i < count($events); ++$i) {
            DB::table('suggested_categories')->insert($this->fakeSuggestedCategory($faker, $events[$i]['id']));
            $bar->advance();
        }
        $bar->finish();
        $this->command->info('');
    }

    public function fakeSuggestedCategory(Faker\Generator $faker, $event_id)
    {
        return [
            'name' => $faker->name,
            'event_id' => $event_id
        ];
    }
}
