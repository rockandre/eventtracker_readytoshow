<?php

use Illuminate\Database\Seeder;

class Event_TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$events = DB::table('events')->pluck('id')->toArray();
    	$tags = DB::table('tags')->pluck('id')->toArray();

    	$faker = Faker\Factory::create('pt_PT');

    	$tagCounter = App\Tag::all();

    	foreach ($tagCounter as $tagZero) {
    		$tagZero->counter = 0;
    		$tagZero->save();
    	}


    	$this->command->info('Assigning tags to events ...');
    	$bar = $this->command->getOutput()->createProgressBar(count($events));
    	for ($i = 0; $i < count($events); ++$i) {
    		$numberTags = $faker->numberBetween(1, 3);

    		$tagsUsed = array();
    		for($j = 0 ; $j < $numberTags ; $j++) {
    			$tag = $tags[$faker->numberBetween(0, (count($tags)-1))];
    			if(!in_array($tag, $tagsUsed)) {
    				DB::table('event_tag')->insert(['event_id' => $events[$i], 'tag_id' => $tag]);
    				array_push($tagsUsed, $tag);
    				$tagCounter = App\Tag::findOrFail($tag);
    				$tagCounter->counter += 1;
    				$tagCounter->save();
    			} else {
    				$j--;
    			}
    		}
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');
    }
}
