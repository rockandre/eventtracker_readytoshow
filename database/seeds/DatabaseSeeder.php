<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //FACTORIES

        //Random "Categories"
    	//factory(App\Category::class, 20)->create();

        //Random "Tags" with counter between 0 and 100
        //factory(App\Tag::class, 30)->create();


        //Random "Events"
        //factory(App\Event::class, 100)->create();

        //Random "Users"
        //factory(App\User::class, 200)->create();

        //SEEDERS
        $this->call(CategoriesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(CancellationsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(Event_TagTableSeeder::class);
        $this->call(Event_FollowersTableSeeder::class);
        $this->call(EvaluationsTableSeeder::class);
        $this->call(ReportsTableSeeder::class);
        $this->call(MultimediaTableSeeder::class);
        $this->call(Suggested_CategoriesTableSeeder::class);
        $this->call(Ticket_Sales_PointsTableSeeder::class);
        $this->call(User_EvaluationsTableSeeder::class);
    }
}
