<?php

use Illuminate\Database\Seeder;
use Intervention\Image\ImageManagerStatic as Image;

class EventsTableSeeder extends Seeder
{
	// Change these properties to reflect student's use of status fields
	const ADVERTISED = 1;
	const CANCELLATION = 2;
	const CANCELED = 3;
	const IN_EVAL = 4;
	const FINISHED = 5;

	private $status = [self::ADVERTISED, self::CANCELLATION, self::CANCELED, self::IN_EVAL, self::FINISHED];

	private $photoPath = 'public/event';
	private $photoListPath = 'public/event_list';
	private $numberOfEvents = 35;
	private $numberOfHiddenEvents = 5;
	private $numberOfImages = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->command->table(['Events table seeder notice'], [
    		['Events images will be stored on path '.storage_path('app/'.$this->photoPath).' and on path '.storage_path('app/'.$this->photoListPath)],
    		['Edit this file to change the storage path or the number of events to generate']
    		]);

    	if ($this->command->confirm('Do you wish to delete previous events files from '.storage_path('app/'.$this->photoPath).' and '.storage_path('app/'.$this->photoListPath).'?', true)) {
    		Storage::deleteDirectory($this->photoPath, true);
    		Storage::deleteDirectory($this->photoListPath, true);
    	}
    	Storage::makeDirectory($this->photoPath);
    	Storage::makeDirectory($this->photoListPath);

        // Disclaimer: I'm using faker here because Model classes are developed by students
    	$faker = Faker\Factory::create('pt_PT');

    	$this->command->info('Downloading Images for events...');
    	$images = $this->getImages($faker);
    	$this->command->info('');

    	$normalUsers = DB::table('users')->where('is_admin', false)->pluck('id')->toArray();
    	$districts = DB::table('districts')->pluck('id')->toArray();
    	$categories = DB::table('categories')->pluck('id')->toArray();
    	array_splice($categories, 0, 1);
    	array_splice($categories, (count($categories)-1), 1);

    	$this->command->info('Creating '.$this->numberOfEvents.' events...');
    	$bar = $this->command->getOutput()->createProgressBar($this->numberOfEvents);
    	for ($i = 0; $i < $this->numberOfEvents; ++$i) {
    		DB::table('events')->insert($this->fakeEvent($faker, $faker->randomElement($normalUsers), 
    			$faker->randomElement($districts), $faker->randomElement($categories),
    			$faker->randomElement($this->status), $faker->randomElement($images)));
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');

    	$this->command->info('Creating '.$this->numberOfHiddenEvents.' hidden events...');
    	$bar = $this->command->getOutput()->createProgressBar($this->numberOfHiddenEvents);
    	for ($i = 0; $i < $this->numberOfHiddenEvents; ++$i) {
    		$event =  $this->fakeEvent($faker, $faker->randomElement($normalUsers), 
    			$faker->randomElement($districts), $faker->randomElement($categories),
    			$faker->randomElement($this->status), $faker->randomElement($images));
    		DB::table('events')->insert($event);
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');

    	copy(storage_path('app/public/example.jpg'), storage_path('app/'.$this->photoPath.'/example.jpg'));
    	copy(storage_path('app/public/example.jpg'), storage_path('app/'.$this->photoListPath.'/example.jpg'));

    }

    private function fakeEvent(Faker\Generator $faker, $user_id, $district_id, $category_id, $status, $image_id)
    {
    	$createdAt = Carbon\Carbon::now()->subDays($faker->numberBetween(1,20));
    	$updatedAt = $faker->dateTimeBetween($createdAt, Carbon\Carbon::now());

    	$initDate = Carbon\Carbon::now()->addDays($faker->numberBetween(1,200));
    	$finishDate = Carbon\Carbon::parse($initDate)->addHours($faker->numberBetween(1,150))->addMinutes($faker->numberBetween(1,60))->addSeconds($faker->numberBetween(1,60));

    	$hasTickets = $faker->numberBetween(0,1);

    	if($hasTickets) {
    		$ticketsForSale = $faker->numberBetween(100, 100000);
    		$ticketManagement = $faker->randomElement(['external', 'eventTracker']);
    		if($ticketManagement == 'external') {
    			$ticketSaleDescription = $faker->randomElement([null, $faker->realText]);
    		}
    	}

    	$evalTime = $faker->randomElement(['7', '14', '21', '1M']);

    	switch ($evalTime) {
    		case '7':
    		$finalEvalDay = Carbon\Carbon::parse($finishDate)->addDays(7);
    		break;
    		case '14':
    		$finalEvalDay = Carbon\Carbon::parse($finishDate)->addDays(14);
    		break;
    		case '21':
    		$finalEvalDay = Carbon\Carbon::parse($finishDate)->addDays(21);
    		break;
    		default:
    		$finalEvalDay = Carbon\Carbon::parse($finishDate)->addMonth();
    		break;
    	}

    	$category = App\Category::findOrFail($category_id);
    	$category->counter += 1;
    	$category->save(); 

    	return [
    	'status' => $status,
    	'name' => $faker->name,
    	'initDate' => $initDate,
    	'finishDate' => $finishDate,
    	'description' => $faker->realText,
    	'spaceName' => $faker->randomElement([null, $faker->streetName]),
    	'address' => $faker->randomElement([null, $faker->address]),
    	'lat' => $faker->randomFloat(5, -85.05115, 85.05115),
    	'lng' => $faker->randomFloat(5, -180, 180),
    	'hasTickets' => $hasTickets,
    	'ticketsForSale' => isset($ticketsForSale) ? $ticketsForSale : null,
    	'ticketManagement' => isset($ticketManagement) ? $ticketManagement : null,
    	'ticketSaleDescription' => isset($ticketSaleDescription) ? $ticketSaleDescription : null,
    	'evalType' => $faker->randomElement(['free', 'conditional']),
    	'evalTime' => $evalTime,
    	'finalEvalDay' => $finalEvalDay,
    	'district_id' => $district_id,
    	'category_id' => $faker->randomElement([null, $category_id, $category_id, $category_id, $category_id]),
    	'image_id' => $faker->randomElement(['example.jpg', $image_id, $image_id]),
    	'user_id' => $user_id,
    	'hidden' => false,
    	'counter_views' => $faker->numberBetween(0, 20000),
    	'created_at' => $createdAt,
    	'updated_at' => $updatedAt
    	];
    }

    private function newEventImage(Faker\Generator $faker)
    {
    	$imageUrl = $faker->imageUrl(1280, 960, $faker->randomElement(['sports', 'city', 'nightlife', 'business', 'food', 'fashion', 'nature', 'technics', 'transport']), false);

    	$filename = time().'.png';

    	try{

    		$image = Image::make($imageUrl);
    		$image->save(storage_path('/app/public/event/' . $filename), 100);
    		$image->heighten(200);
    		$image->save(storage_path('/app/public/event_list/' . $filename), 100);
    	} catch(Exception $e) {
    		return "example.jpg";
    	}
    	

    	return $filename;

    }

    private function getImages(Faker\Generator $faker)
    {
    	$photoNames = array();

    	$bar = $this->command->getOutput()->createProgressBar($this->numberOfImages);
    	for ($i=0; $i < $this->numberOfImages; $i++) { 
    		array_push($photoNames, $this->newEventImage($faker));
    		$bar->advance();
    	}
    	$bar->finish();

    	return $photoNames;
    }
}