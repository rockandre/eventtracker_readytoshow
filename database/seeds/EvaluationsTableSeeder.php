<?php

use Illuminate\Database\Seeder;

class EvaluationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$events = App\Event::whereIn('status', [4, 5])->where('evalType', 'free')->get()->toArray();
    	$numberMaxEvals = DB::table('users')->count();

    	$faker = Faker\Factory::create('pt_PT');

    	$this->command->info('Adding evaluations to events ...');
    	$bar = $this->command->getOutput()->createProgressBar(count($events));
    	for ($i = 0; $i < count($events); ++$i) {
    		$numberEvals = $faker->numberBetween(1, 20);
    		for($j = 0 ; $j < $numberEvals ; $j++) {
    			DB::table('evaluations')->insert($this->fakeEvaluation($faker, $events[$i]['id'], $events[$i]['user_id']));
    		} 
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');
    }

    private function fakeEvaluation(Faker\Generator $faker, $event_id, $promotor_id) 
    {
    	$createdAt = Carbon\Carbon::now()->subDays($faker->numberBetween(1,20));

    	return [
    		'event_id' => $event_id,
    		'promotor_id' => $promotor_id,
    		'eventEval' => $faker->numberBetween(1, 5),
    		'eventOpinion' => $faker->randomElement([null, $faker->realText]),
    		'promotorEval' => $faker->numberBetween(1, 5),
    		'promotorOpinion' => $faker->randomElement([null, $faker->realText]),
    		'created_at' => $createdAt,
    		'updated_at' => $createdAt
    	];
    }

}
