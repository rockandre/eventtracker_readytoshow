<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    
    return [
    	'name' => $faker->name,
    	'email' => $faker->email,
    	'password' => bcrypt('1234567890'),
    ];
});
