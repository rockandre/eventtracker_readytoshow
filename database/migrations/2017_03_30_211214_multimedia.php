<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Multimedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('multimedia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150);
            $table->string('url', 2500);

            $table->integer('event_id')->unsigned();

            $table->foreign('event_id')->references('id')->on('events');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multimedia');
    }
}
