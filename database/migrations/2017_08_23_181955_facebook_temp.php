<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacebookTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('facebook_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider_user_id');
            $table->string('name', 75);
            $table->string('given_email')->nullable();
            $table->timestamps();
            $table->string('email_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_temp');
    }
}
