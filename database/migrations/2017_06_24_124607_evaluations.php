<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Evaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('promotor_id')->unsigned();
            $table->tinyInteger('eventEval')->unsigned();
            $table->text('eventOpinion')->nullable();
            $table->tinyInteger('promotorEval')->unsigned();
            $table->text('promotorOpinion')->nullable();
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('promotor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
