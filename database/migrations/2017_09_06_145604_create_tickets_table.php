<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_type_id')->unsigned();;
            $table->integer('order_id')->unsigned();
            $table->string('email')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('identification_number');
            $table->boolean('transmissable')->default(0);
            $table->string('status', 1)->default(1);
            $table->string('private_code', 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
