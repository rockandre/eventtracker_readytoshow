<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');
             
            $table->string('status', 1)->default(1);
            $table->string('name',  255);
            $table->dateTime('initDate');
            $table->dateTime('finishDate');
            $table->text('description');
            
            $table->string('spaceName', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->float('lat', 10, 6);
            $table->float('lng', 10, 6);
            $table->boolean('hasTickets');
            $table->mediumInteger('ticketsForSale')->nullable();
            $table->string('ticketManagement', 12)->nullable();
            $table->text('ticketSaleDescription')->nullable();
            $table->string('evalType', 11);
            $table->string('evalTime',2);
            $table->dateTime('finalEvalDay');
            $table->integer('district_id')->unsigned();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('image_id', 38)->default('example.jpg');
            $table->integer('user_id')->unsigned();
            $table->boolean('hidden')->default(false);
            $table->integer('counter_views')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('user_id')->references('id')->on('users');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
