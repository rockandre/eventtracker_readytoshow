<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('name', 125);
            $table->mediumInteger('quantity');
            $table->unsignedSmallInteger('price');
            $table->unsignedTinyInteger('ageRestrictionMin')->nullable();
            $table->unsignedTinyInteger('ageRestrictionMax')->nullable();
            $table->boolean('is_standard');
            $table->timestamps();


            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_types');
    }
}
