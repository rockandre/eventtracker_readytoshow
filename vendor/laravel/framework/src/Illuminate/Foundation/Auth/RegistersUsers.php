<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Mail\MailConfirmation;
use Illuminate\Support\Facades\Mail;

use Intervention\Image\ImageManagerStatic as Image;
use App\User;
use App\District;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $districts = District::all();

        return view('auth.register', compact('districts'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {   
        $validator = $this->validator($request->all());

        $socialAccountCheck = $this->checkForSocialAccount($request->get('email'));

        $accountNotConfirmed = $this->checkAccountRegistedButNotConfirmed($request->get('email'));

        if($validator->fails()) {
            $errorBag = $validator->errors();

            if(!is_null($socialAccountCheck))
            {
                $errorBag->add('socialAccount', $socialAccountCheck);
            }

            if(!is_null($accountNotConfirmed))
            {
                $errorBag->add('emailNotConfirmed', $accountNotConfirmed);
            }

            return response()->json($errorBag, 422);
            
        }

        event(new Registered($user = $this->create($request->all())));

        if(!is_null($request['profileImage']))
        {   
            $user->image_id = $this->generateProfileImage($user->id, $request->get('profileImage'));
            $user->save();
        }

        $user->email_token = str_random(25);
        $user->save();

        Mail::to($user->email)->send(new MailConfirmation($user));

        return $this->registered($request, $user)
        ? redirect('login') : redirect($this->redirectPath());
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if($user) {
            return true;
        }
        else {
            return false;
        }
    }


    protected function checkForSocialAccount($email)
    {
        $user = User::where([['email', $email], ['social_account', '>', 0]])->first();

        if(count($user))
        {
            $socialAccountType = $user->social_account == 1 ? 'Facebook' : 'Google';

            return 'O utilizador ' . $user->name . ' já se registou com uma conta da rede social ' . $socialAccountType;
        }

        return null;
    }

    protected function checkAccountRegistedButNotConfirmed($email)
    {
        $user = User::where([['email', $email], ['confirmed', 0]])->first();

        if(count($user))
        {
            $user->email_token = str_random(25);
            $user->save();

            Mail::to($user->email)->send(new MailConfirmation($user));

            return 'O utilizador ' . $user->name . ' já se registou! Para finalizar o registo basta confirmar o email. Voltamos a mandar um email de confirmação!';
        }

        return null;
    }

    protected function generateProfileImage($userId, $image)
    {

        $fileName = $userId . '_' . time() . '.png';

        $image = Image::make($image);
        $image->heighten(400);  
        $image->save(storage_path('/app/public/profile/' . $fileName), 100);

        return $fileName;
    }
}